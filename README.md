# TransKey

## Installation

You can run the server, directly on our workstation:

* required python>=3.11 and pip 3

```bash
> python -m venv venv
> source venv/bin/activate
> pip install -r requirements.txt
```

Or building a docker image:

```bash
> docker build -t transkey .
```
  
docker run --name transkey -p 5000:5000 -it --rm 

## Getting started

### Launch the server

On the workstation:

```bash
> cd sources/
> python serveur.py -l
```

Using the docker image:

```bash
docker run --name transkey -p 5000:5000 -it --rm transkey
```
### Browser web connect

* Enter http://localhost:5000/ on our web browser.
* Choose the configuration `../config_game_4p` or `../config_game_5p`
* launch the game,...

some [game rules](./regles.md)

## Code

* [Info. about the conception](./nodes.md) in french
* [Rest and socket API](.api.md)
  

### Files

* `config_game_4p`, `config_game_5p`: game configurations
* `tests_configs`: tests configuration
* `sources`: flask and iosocket server side (in python)
* `web_client`: client side i.e flask template folder (html/javascript)
