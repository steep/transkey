const urlParams = new URLSearchParams(window.location.search);

const ip = window.location.host;
var sio = io.connect(ip);

var turnDone;
var currentTurn;
var currentPhase;
var nbPlayersDone = 0;
var nbPlayers = 0;
const trades = new Trades();
var playerRes = {};

function getPlayerRes() {return playerRes;}

// "Static" config, containing things that should not change after load
// Easier to pass as only one argument to get all constant data
var staticConfig = {
    playerName: urlParams.get("player_name"),
    resCat: [],  // Resources categories
    resInfo: {}, // Resources info (images, visible, ...)
    actInfo: {}, // Activities info (images)
    playersList: {},
    konvaBoard: {},
    visibleInfo: {},
    fixRoadCost: {},
    desc: "",
    individual_objective: "",
    shared_objective: "",
}

/* LOAD GAME AND STATIC CONFIG SETUP */

function gameStart() {
    // Request players list
    sio.emit("get_config");
    sio.emit("get_desc", staticConfig.playerName);
    sio.emit("get_objectives", staticConfig.playerName);

    if (urlParams.get("admin_view")) {
        staticConfig.playerName = "ADMIN"; // Change name for chat
        // Hide useless elements
        document.getElementById("objectives").classList.add("hide");
        document.getElementById("territory").classList.add("hide");
        document.getElementById("button_next_phase").classList.add("hide");
        document.getElementById("button_new_trade").classList.add("hide");
    } else {
        // Show player name
        const node = document.getElementById("player_name_display");
        node.textContent = getText("welcome") + ", " + staticConfig.playerName;
    }

    // Setup canvas
    staticConfig.konvaBoard = initCanvas();
}

sio.on("game_start", () => gameStart());

window.onload = () => {
    loadTexts();
    // Press enter when typing in chat
    document.getElementById("message").addEventListener("keydown", (e) => {
        if (e.key === "Enter") sendMsg();
    });
    gameStart();

    addProgressBar(document.getElementById("player_actions_display_bar"), 0, "");
    addProgressBar(document.getElementById("phase_display_bar"), 0, "");
    addProgressBar(document.getElementById("turn_display_bar"), 0, "");
    addProgressBar(document.getElementById("finished_players_bar"), 0, "");

    document.getElementById("phase_display_bar").onclick = phasePopup;
};

sio.on("get_config", (playersList, visInfo) => {
    staticConfig.playersList = playersList;
    staticConfig.visibleInfo = new Set(visInfo);

    // Setup canvas and infos
    setupBoard(staticConfig);
    sio.emit("get_info", staticConfig.playerName);
});

sio.on("get_fix_road_cost", (recipe) => staticConfig.fixRoadCost = recipe);

sio.on("get_activities_info", (act) => {
    staticConfig.actInfo = JSON.parse(act);
    if (urlParams.get("admin_view")) showRecipes({}, true);
});

sio.on("get_resources_info", (cat, res) => {
    staticConfig.resCat = cat;
    staticConfig.resInfo = JSON.parse(res);
})

sio.on("get_desc", (desc) => staticConfig.desc = desc);

function showDesc() {
    const node = openPopup(getText("territory_desc_popup"));
    const textNode = addText(node, staticConfig.desc);
    textNode.classList.add("text_list");
}

sio.on("get_objectives", (shared, individual) => {
    staticConfig.shared_objective = shared;
    staticConfig.individual_objective = individual;
});

function showIndividualObjective() {
    const node = openPopup(getText("individual_objective_popup"));
    const textNode = addText(node, staticConfig.individual_objective);
    textNode.classList.add("text_list");
}

function showSharedObjective() {
    const node = openPopup(getText("shared_objective_popup"));
    const textNode = addText(node, staticConfig.shared_objective);
    textNode.classList.add("text_list");
}

/* CHAT AND LOG MESSAGE HANDLER */

function logMsg(msg) {
    const node = document.getElementById("chat_messages");
    addText(node, msg);
    const chatCell = document.getElementById("chat_cell");
    chatCell.scrollTo(0, node.scrollHeight);
}

sio.on("log", (msg) => {
    if (urlParams.get("admin_view")) return;

    logMsg(msg);
    notifCol("button_col_left", isLeftColFolded());
});

function sendMsg() {
    const msg = document.getElementById("message").value;
    if (msg) {
        document.getElementById("message").value = "";
        sio.emit("send_msg", staticConfig.playerName, msg);
    }
}

sio.on("recv_msg", (playerName, msg) => {
   logMsg(playerName + " : " + msg);
});

/* ACTIONS (AMENAGEMENTS) */

var nbActions=0, nbActionsTotal=0;
function updateNbActions(done, total) {
    nbActions = done;
    nbActionsTotal = total;
    const playerActionsBar = document.getElementById("player_actions_display_bar").firstChild;
    updateProgressBar(playerActionsBar, nbActions/nbActionsTotal, getText("progress_bar_actions") + ` ${nbActions} / ${nbActionsTotal}`);
}

sio.on("get_nb_actions", (values) => updateNbActions(values[0], values[1]));

/* RESOURCES */

sio.on("get_resources", (playerName, res) => {
    if (staticConfig.visibleInfo.has("other resources")) updateResources(playerName, staticConfig.resInfo, res);

    if (playerName === staticConfig.playerName) {
        const node = cleanNode("resources_cell");
        playerRes = res;
        displayResources(node, staticConfig.resCat, staticConfig.resInfo, playerRes);

        const buttonHousing = addButton(node.lastChild.lastChild.childNodes[1], getText("housing_button"), "house()");
        buttonHousing.classList.add("housing_button");

        const buttonConvert = addButton(node.lastChild.lastChild.lastChild, getText("convert_button"), "convertFood()");
        buttonConvert.classList.add("convert_button");

    }

    nbHoused = playerRes["housed employee"];
});

/* HOUSING */

var nbHoused = 0;

function house() {
    const node = openPopup(getText("housing_employees_popup"));
    displayHousing(node, staticConfig, nbHoused, Math.min(playerRes["housing"], playerRes["employee"]));
}

function updateHoused(val) {
    nbHoused = Math.max(0, Math.min(val, Math.min(playerRes["housing"], playerRes["employee"])));
    house();
}

/* RATIONS */

var conv = {}; // Temp values to be converted into rations

function convertFood() {
    const node = openPopup(getText("convert_food_popup"));
    displayResRation(node, staticConfig, playerRes, conv);
}

function updateConvertResource(r, val) {
    conv[r] = Math.max(0, Math.min(val, playerRes[r]));
    convertFood();
}

/* RECIPES */

function addActivity(activityName) {
    sio.emit("add_activity", staticConfig.playerName, activityName);
}

// Show all recipes accessible to player. If activity not allowed, put in gray.
// NOTE : admin view only shows recipes containing something for "all"
function showRecipes(recipesList, adminView) {
    const node = cleanNode("recipes");

    let counter = 0;
    // Put a div for each recipe
    for (const [key, attributes] of Object.entries(staticConfig.actInfo)) {
        // If player doesn't have recipe + No base_recipe, skip
        if (!(recipesList[key] || attributes.base_recipe)) continue;

        const newBox = document.createElement("div");
        newBox.setAttribute("id", "recipe_" + counter);
        newBox.setAttribute("title", key);
        newBox.style.backgroundColor = staticConfig.actInfo[key].color;

        const img = document.createElement("img");
        img.setAttribute("src", staticConfig.actInfo[key].image_path);

        // Darken image if activity can't be added by player
        if (!recipesList[key] && !adminView) {
            img.classList.add("darken");
            newBox.classList.add("darken");
        }

        newBox.appendChild(img);

        node.appendChild(newBox);

        const recipeHover = document.createElement("div");

        addTextH(recipeHover, getText("top_text_recipe"), 2);


        if (recipesList[key]) {  // If player has received recipe, allowed to add
            // Button to add activity
            addButton(newBox, getText("add_activity_button"), `addActivity("${key}")`, "add_recipe_" + counter);
            displayRecipe(recipeHover, staticConfig, key, recipesList[key]);
        } else {  // Else the recipe has a base_recipe, display it
            displayRecipe(recipeHover, staticConfig, key, JSON.parse(attributes.base_recipe));
        }
        node.appendChild(recipeHover);
        recipeHover.classList.add("recipe_hover");

        counter++;
    }
}

sio.on("get_recipes", (res) => showRecipes(JSON.parse(res), false));

/* TERRAINS */

function setActivityState(id, state) {
    sio.emit("set_activity_state", staticConfig.playerName, id, state);
}

sio.on("get_terrains", (playerName, terrains, size) => updateTerrains(staticConfig, playerName, JSON.parse(terrains), size));

// Popup to interact with activity
function activityInteraction(staticConfig, terrainOwner, id, activity) {
    const node = openPopup(getText("edit_activity_popup"));
    const info = JSON.parse(activity.info);

    displayRecipe(node, staticConfig, activity.name, info);

    // Player can interact with its own terrains
    if (terrainOwner === staticConfig.playerName) {
        // Add "turn on" button if activity off
        if (!activity.running) {
            if (info.auto) {
                addText(node, getText("auto_on_activity"));
            } else {
                const but = addButton(node, getText("turn_on_activity_button"), `sio.emit("set_activity_state","${terrainOwner}",${id},"on");closePopup();`);
                // If player is missing resources when opening popup, turn button gray
                if (!hasEnoughRes(getPlayerRes(), info.turn_req)) but.classList.add("dark_button");
            } 
        }

        // "Remove"
        if (!activity.new) {
            const but = addButton(node, getText("remove_activity_button"), `sio.emit("remove_activity","${terrainOwner}",${id});closePopup();`);
            // If player is missing actions or activity running, turn button gray
            if (nbActions === nbActionsTotal || activity.running) but.classList.add("dark_button");
        }
    }
}

/* TRADES */

function newTradePopup() {
    const node = openPopup(getText("begin_new_trade_popup"));
    addText(node, getText("trade_begin_with_who"));

    // For each other player, add button
    for (const player of staticConfig.playersList) {
        if (player === staticConfig.playerName) continue;
        addButton(node, player, `newTrade("${player}")`);
    }
}

function newTrade(player) {
    sio.emit("start_trade", staticConfig.playerName, player, {}, {});
}

function setTradeState(tradeId, state) {
    sio.emit("set_trade_state", staticConfig.playerName, tradeId, state);
}

function updateTrade(tradeId) {
    const trade = trades.getTrade(tradeId);
    trades.addSeenTrade(tradeId);

    const currentPlayer = (staticConfig.playerName === trade.players[0]) ? 0 : 1;

    sio.emit("update_trade", tradeId, staticConfig.playerName, trade.resources[currentPlayer], trade.resources[1-currentPlayer]);
}

function updateTradeResource(tradeId, playerBoolean, res, newValue) {
    const tradesRes = trades.getTrade(tradeId).resources[playerBoolean];
    
    tradesRes[res] = Math.max(0, newValue); // Can't have negative values  

    // In the case of current player, can't have more than player's resources
    if (staticConfig.playerName === trades.getTrade(tradeId).players[playerBoolean]) {
        tradesRes[res] = Math.min(tradesRes[res], playerRes[res]);
    }

    updateTrade(tradeId);
}

function openPopupTrades(tradeId) {
    // Update state of what player has seen
    trades.addSeenTrade(tradeId);

    setCurrentTradeId(tradeId);

    const trade = trades.getTrade(tradeId);
    // Trade doesn't exist anymore
    if (trades.isDone(tradeId) !== "") {
        closePopup();
        return;
    }

    const buttonNode = document.getElementById(`but_trade_${tradeId}`);
    if (buttonNode.getAttribute("class") === "trade_unseen") {
        buttonNode.setAttribute("class", "trade_seen");
        document.getElementById(`trade_${tradeId}`).textContent = "";
        checkIfTradeNotif();
    }

    const currentPlayer = (staticConfig.playerName === trade.players[0]) ? 0 : 1;

    const node = openPopup(getText("trade_with") + " " + trade.players[1-currentPlayer]);

    displayResourcesTrades(node, false, {}, staticConfig.resInfo, trade, 1-currentPlayer);
    displayResourcesTrades(node, true, playerRes, staticConfig.resInfo, trade, currentPlayer);

    addButton(node, getText("cancel_trade"), `setTradeState(${trade.id},"KO")`, "cancel_" + trade.id);
}

sio.on("get_trades", (tradesDict) => {
    trades.refresh(tradesDict);
    const node = cleanNode("trades");
    addTextH(node, getText("current_trades"), 3);
    let newPopup = false;  // Check if a popup already happened before displaying trade

    for (const [trade, hasNewInfo, isNewTrade, isDone] of trades.iterator(staticConfig.playerName)) {
        
        const currentPlayer = (staticConfig.playerName === trade.players[0]) ? 0 : 1;

        // If trade ends
        if (isDone === "OK") {
            const node = openPopup(getText("successful_trade_with") + " " + trade.players[1-currentPlayer]);
            newPopup = true;
            trades.addSeenTrade(trade.id);
            
            addText(node, getText("resources_you_gave_to") + " " + trade.players[1-currentPlayer]);
            const tradeInfo1 = document.createElement("div");
            displayPositiveResources(tradeInfo1, staticConfig.resInfo, trade.resources[currentPlayer]);
            node.appendChild(tradeInfo1);
            
            addText(node, getText("resources_given_to_you_by") + " " + trade.players[1-currentPlayer]);
            const tradeInfo2 = document.createElement("div");
            displayPositiveResources(tradeInfo2, staticConfig.resInfo, trade.resources[1-currentPlayer]);
            node.appendChild(tradeInfo2);
            continue;
        } else if (isDone === "KO") {
            const node = openPopup(getText("trade_abort_popup"));
            newPopup = true;
            trades.addSeenTrade(trade.id);
            
            addText(node, getText("the_trade_with") + " " + trade.players[1-currentPlayer] + " " + getText("was_canceled"));
            addButton(node, getText("ok_button"), "closePopup()");
            continue;
        }

        const tradeNode = document.createElement("div");
        tradeNode.classList.add("trade_display");

        const buttonNode = addButton(tradeNode, getText("trade_with") + " " + trade.players[1-currentPlayer], `openPopupTrades(${trade.id},${currentPlayer})`, `but_trade_${trade.id}`);

        const textNode = addText(tradeNode, "");
        textNode.setAttribute("id", `trade_${trade.id}`);
        textNode.classList.add("trade_text");

        // If has new info & not in current popup & not already accepted
        if (hasNewInfo && trade.id !== getCurrentTradeId(trades) && trade.status[currentPlayer] !== "OK") {
            buttonNode.setAttribute("class", "trade_unseen");
            textNode.textContent = "(" + getText("notif_trade_updated") + ")";
        } else if (trade.status[currentPlayer] === "PENDING") {
            buttonNode.setAttribute("class", "trade_seen");
        } else {
            buttonNode.setAttribute("class", "trade_ready");
            trades.addSeenTrade(trade.id);
            textNode.textContent = "(" + getText("notif_trade_waiting_for") + " " + trade.players[1-currentPlayer] + ")";
        }
        // If new trade begins
        if (isNewTrade && trade.status[currentPlayer] !== "OK") {
            const node = openPopup(getText("new_trade_popup"));
            addText(node, getText("trade_beginning_with") + " " + trade.players[1-currentPlayer]);
            addButton(node, getText("check_trade"), `openPopupTrades(${trade.id},${currentPlayer})`);
        }

        node.appendChild(tradeNode);
    }
    if (getCurrentTradeId(trades) !== -1 && !newPopup) openPopupTrades(getCurrentTradeId(trades));
    else checkIfTradeNotif();
});

// Everyone receives notification about trades and ask update for given player
sio.on("info_trade", () => sio.emit("get_trades", staticConfig.playerName));

// Roads
sio.on("get_broken_roads", (brokenList) => updateRoads(staticConfig, brokenList));

/* TURN INFO & NEXT PHASE */

sio.on("get_turn_info", (phase, turn, totalTurns) => {
    currentTurn = turn;
    currentPhase = phase;

    const progressBarPhase = document.getElementById("phase_display_bar").firstChild;

    updateProgressBar(progressBarPhase, currentPhase/4, `${getText("current_phase")} ${currentPhase==0 ? getText("phase_idle") :
                                                                                        currentPhase==1 ? getText("phase_hazards") :
                                                                                        currentPhase==2 ? getText("phase_water") :
                                                                                        getText("phase_main")}`);

    const progressBarTurn = document.getElementById("turn_display_bar").firstChild;
    if (currentTurn > totalTurns) updateProgressBar(progressBarTurn, 1, getText("game_finished"));
    else updateProgressBar(progressBarTurn, (currentTurn-1)/totalTurns, getText("turn") + ` ${currentTurn} / ${totalTurns}`);

    phasePopup();
});

// Popup message with information about current phase
function phasePopup() {
    if (currentPhase === 0) {  // IDLE
        const popupNode = openPopup(getText("phase_idle"));
        const text = addText(popupNode, getText("phase_idle_tuto"));
        text.classList.add("text_list");
    } else if (currentPhase === 1) {  // HAZARDS
        const popupNode = openPopup(getText("phase_hazards"));
        addText(popupNode, getText("phase_hazards_tuto"));
    } else if (currentPhase === 2) {  // WATER
        const popupNode = openPopup(getText("phase_water"));
        addText(popupNode, getText("phase_water_tuto"));
    } else {  // MAIN
        const popupNode = openPopup(getText("phase_main"));
        const text = addText(popupNode, getText("phase_main_tuto"));
        text.classList.add("text_list");
    }
}

function updateTurnTxt() {
    const button = document.getElementById("button_next_phase");
    button.innerText = turnDone ? getText("unfinish_phase") : getText("finish_phase");

    const finishedPlayersBar = document.getElementById("finished_players_bar").firstChild;
    updateProgressBar(finishedPlayersBar, nbPlayersDone/nbPlayers, `${nbPlayersDone} / ${nbPlayers} ` + getText("nb_players_ready_for_next_phase"));
}

function switchTurnState() {
    sio.emit("set_phase_state", staticConfig.playerName, !turnDone);
}

sio.on("get_nb_players_done", (done, total) => {
    nbPlayersDone = done;
    nbPlayers = total;
    if (nbPlayersDone === 0) turnDone = false;
    updateTurnTxt();
});

sio.on("get_phase_state", (state) => {
    turnDone = state;
    updateTurnTxt();
});

sio.on("next_phase", (phase) => {
    currentPhase = phase;
    sio.emit("get_info", staticConfig.playerName);
});

/* VISUALIZATIONS */

sio.on("trade_visu", (data) => {
    // Only show visualization on IDLE phase (or anytime if config has terrains visualizations)
    updateVisu(staticConfig, (staticConfig.visibleInfo.has("terrains visualizations") || currentPhase === 0) ? JSON.parse(data) : {});
});

function showVisualizations() {
    const node = openPopup(getText("visu_popup"));
    addText(node, getText("visu_tuto"));

    const div = document.createElement("div");
    div.classList.add("visu_div");

    const grid = document.createElement("div");
    grid.classList.add("visu_grid");

    for (const p of staticConfig.playersList) {
        const line = document.createElement("div");
        line.classList.add("visu_line");
        addText(line, p);
        for (let t=1; t<currentTurn; t++) {
            addButton(line, getText("turn") + " " + t, `window.open("render/${p}_${t}.svg", "_blank")`);
        }
        grid.appendChild(line);
    }
    div.appendChild(grid);
    node.appendChild(div);
}

/* HUD COLUMNS */

function hideLeftCol() {
    const xBefore = document.getElementById("terrains_cell").getBoundingClientRect().x;
    updateColsVisibility("col_1", "collapse", "button_col_left", ">", "showLeftCol()");
    updateCols([-18, 0, 18, 0, 0]);
    const xAfter = document.getElementById("terrains_cell").getBoundingClientRect().x;
    resizeCanvas(staticConfig.konvaBoard, xBefore - xAfter);
}

function showLeftCol() {
    const xBefore = document.getElementById("terrains_cell").getBoundingClientRect().x;
    updateColsVisibility("col_1", "visible", "button_col_left", "<", "hideLeftCol()");
    updateCols([18, 0, -18, 0, 0]);
    const xAfter = document.getElementById("terrains_cell").getBoundingClientRect().x;
    resizeCanvas(staticConfig.konvaBoard, xBefore - xAfter);
    notifCol("button_col_left", false);
}

function hideRightCol() {
    updateColsVisibility("col_3", "collapse", "button_col_right", "<", "showRightCol()");
    updateCols([0, 0, 18, 0, -18]);
    resizeCanvas(staticConfig.konvaBoard, 0);
}

function showRightCol() {
    updateColsVisibility("col_3", "visible", "button_col_right", ">", "hideRightCol()");
    updateCols([0, 0, -18, 0, 18]);
    resizeCanvas(staticConfig.konvaBoard, 0);
}

// Check if any trade is unread and turn on notification if it's the case
function checkIfTradeNotif() {
    if (trades.hasUnseen()) notifCol("button_col_right", true);
    else notifCol("button_col_right", false);
}
