/* This file contains generic functions for DOM / Display and functions shared for different files */

/* CLEAN AND ADD ELEMENT */

// Remove every child from a node return the node element
function cleanNode(node) {
    const res = document.getElementById(node);
    while (res.firstChild) res.removeChild(res.lastChild);
    return res;
}

// Add text (p) child to node and return child element
function addText(node, text) {
    const textContainer = document.createElement("p");
    textContainer.textContent = text;
    node.appendChild(textContainer);
    return textContainer;
}

// Add text (h) child to node and return child element
function addTextH(node, text, nb) {
    const textContainer = document.createElement("h" + nb);
    textContainer.textContent = text;
    node.appendChild(textContainer);
    return textContainer;
}

// Add button child to node and return child element
function addButton(node, buttonText, onclick, id="") {
    const but = document.createElement("button");
    if (id) but.setAttribute("id", id);
    but.innerText = buttonText;
    but.setAttribute("onclick", onclick);
    node.appendChild(but);
    return but;
}

// Add input child to node and return child element
function addInput(node, id, inputText) {
    const but = document.createElement("input");
    but.setAttribute("id", id);
    but.defaultValue = inputText;
    node.appendChild(but);
    return but;
}

// Add option to a select node and return child element
function addOption(node, value) {
    const opt = document.createElement("option");
    opt.value = value;
    opt.innerHTML = value;
    node.appendChild(opt);
    return opt;
}

/* POPUP AND ESCAPE BUTTON */

// Add escape button to node with onclick function to close it
function addEscapeButton(node, onclick) {
    const popupClose = document.createElement("img");
    popupClose.setAttribute("src", "./images/button_cross.svg");
    popupClose.setAttribute("id", "button_close");
    popupClose.setAttribute("onclick", onclick);
    node.appendChild(popupClose);
    return popupClose;
}

// Clean and display popup
function openPopup(title) {
    document.getElementById("popup_overlay").style.display = "block";
    const node = cleanNode("popup_content");
    
    const titleNode = document.createElement("h2");
    titleNode.textContent = title;
    node.appendChild(titleNode);

    node.style.display = "block";

    addEscapeButton(node, "closePopup()");
    return node;
}

// Hide popup
function closePopup() {
    document.getElementById("popup_overlay").style.display = "none";
    document.getElementById("popup_content").style.display = "none";
    resetCurrentTradeId();
}

// Bind escape key to close popup
document.addEventListener("keydown", (e) => {
    if (e.key === "Escape") {
        closePopup();
    }
});

/* PROGRESS BAR */

// Create new progress bar and return it
function addProgressBar(parentNode, ratio, text) {
    const barNode = document.createElement("div");
    barNode.classList.add("progress_bar");

    const fillNode = document.createElement("div");
    fillNode.classList.add("progress_bar_fill");
    barNode.appendChild(fillNode);

    const textNode = document.createElement("div");
    textNode.classList.add("progress_bar_text");
    barNode.appendChild(textNode);

    parentNode.appendChild(barNode);

    updateProgressBar(barNode, ratio, text);
    return barNode;
}

// Change text and completion
function updateProgressBar(node, ratio, text) {
    node.firstChild.style.width = ratio*100 + "%";
    node.lastChild.textContent = text;
};

/* HUD COLUMNS */

function updateColsVisibility(col, visibility, button, text, onclick) {
    document.getElementById(col).style.visibility = visibility;
    document.getElementById(button).textContent = text;
    document.getElementById(button).setAttribute("onclick", onclick);
}

// Columns percentage of pageview
var colPercentages = [18, 2, 60, 2, 18];

// Update columns display
function updateCols(deltaPercentages) {
    // Update col percentages
    colPercentages = colPercentages.map((val, i) => val + deltaPercentages[i]);

    // Create string with css format
    const str = colPercentages.map(val => `${val}%`).join(' ');
    document.getElementById("layout").style.gridTemplateColumns = str;
}

// If column is hidden, put notification on
function notifCol(button, on) {
    document.getElementById(button).setAttribute("class", on ? "button_col_notif" : "button_col");
}

function isLeftColFolded() {
    return colPercentages[0] === 0;
}

/* CHECK RESOURCES */

// Return boolean indicating if player has the resources requested
function hasEnoughRes(playerRes, reqRes) {
    for (const [res, val] of Object.entries(reqRes)) {
        if (playerRes[res] < val) return false;
    }
    return true;
}