var currentTradeId = -1; // If trade popup is visible, which trade should be shown
function resetCurrentTradeId() {currentTradeId = -1;}
function getCurrentTradeId(trades) {
    if (trades.isDone(currentTradeId)) return -1;
    return currentTradeId;
}
function setCurrentTradeId(tradeId) {currentTradeId = tradeId;}

class Trades {
    constructor() {
        this.trades = {}; // Current trades for player
        this.seenTrades = {}; // Last version of trade seen by player (when popup opened)
        this.newTradeNotif = []; // Every trade which had a popup for player

        this.getTrade = function (tradeId) {
            return this.trades[tradeId];
        };

        this.setTrade = function (tradeId, trade) {
            this.trades[tradeId] = trade;
        };

        // Refresh "trades" object by replacing with new values
        this.refresh = function (tradesDict) {
            // Erase every trade
            this.trades = {};
            // Update every trade
            for (let trade of tradesDict) {
                trade = JSON.parse(trade);
                this.setTrade(trade.id, trade);
            }
        }

        // Add a trade to seenTrades as a backup so then it can be checked if something is new
        this.addSeenTrade = function (tradeId) {
            this.seenTrades[tradeId] = this.trades[tradeId];
        }

        // Check if trade is same as seen by player
        // Note: Assumes every resource is in the 'resources' dictionary of the trade, even with a value of 0
        this.notSeenTrade = function (tradeId) {
            const trade = this.trades[tradeId];
            const lastSeen = this.seenTrades[tradeId];

            if (!lastSeen) return true; // Trade never opened

            // Check if status is different
            if (trade.status[0] !== lastSeen.status[0]) return true;
            if (trade.status[1] !== lastSeen.status[1]) return true;

            // Check amount in current trade is same as backup
            for (let i=0; i<2; i++) {
                for (const [res, val] of Object.entries(trade.resources[i])) {
                    // If different amount, trades are different
                    if (val !== lastSeen.resources[i][res]) return true;
                }
            }
            return false;
        }

        // Check it's the first time the player sees a trade
        this.isNewTrade = function (tradeId) {
            if (!(this.newTradeNotif.includes(tradeId)) &&  // Not notified before
                !(this.seenTrades[tradeId])) { // Not own accepted trade
                this.newTradeNotif.push(tradeId);
                return true;
            }
            return false;
        }

        this.isDone = function (tradeId) {
            const trade = this.trades[tradeId];
            // Trade already deleted
            if (!trade) return "KO";
            // KO
            if (trade.status[0] === "KO") return "KO";
            // OK
            if (trade.status[0] === "OK" && trade.status[1] === "OK") return "OK";
            // Pending
            return "";
        }


        this.iterator = function* () {
            for (const [key, value] of Object.entries(this.trades)) {
                yield [value, this.notSeenTrade(key), this.isNewTrade(key), this.isDone(key)];
            }
        }
        
        // Return true if player has any notSeenTrade
        this.hasUnseen = function () {
            for (const [key, _] of Object.entries(this.trades)) {
                // At least one trade is unread
                if (this.notSeenTrade(key)) {
                    return true;
                }
            }
            return false;
        }
    }
}
