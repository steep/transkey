/* Set all texts to fit language requested by player */

// Code inspired from https://medium.com/@nohanabil/building-a-multilingual-static-website-a-step-by-step-guide-7af238cc8505
// To avoid async issues (promises etc.) reload page when object is fetch in localStorage

// "lang" is an url argument or english by default
var lang = new URLSearchParams(window.location.search).get("lang") ?? "en";

// function setLang(newLang) {
//     lang = newLang;
// }

function loadTexts() {
    const data = getLangFile();

    document.querySelectorAll('[data-i18n]').forEach(element => {
        const key = element.getAttribute('data-i18n');
        element.textContent = data[key] ? data[key] : key;
    });
}

function getText(textId) {
    const data = getLangFile();
    return data[textId] ? data[textId] : textId;
}

// async function updateText(element) {
//     const data = await getLangFile();

//     const key = element.getAttribute('data-i18n');
//     element.textContent = data[key];
// }

function getLangFile() {
    // Check and return if in local storage
    let data = localStorage.getItem(lang);
    if (data) return JSON.parse(data);

    const filename = "languages/" + ((lang === "fr") ? "fr.json" : "en.json");

    fetch(filename)
        .then((response) => response.json())
        .then((json) => {
            localStorage.setItem(lang, JSON.stringify(json));
            location.reload();
        });   
}
