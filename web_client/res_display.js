/* This file contains functions for resources display */

// Display a resource info (name + image) inside a new div which is returned
function displayResource(res, image, info) {
    const newBox = document.createElement("div");
    newBox.classList.add("resource_display");

    const img = document.createElement("img");
    img.setAttribute("title", res);
    img.setAttribute("src", image);
    newBox.appendChild(img);
    
    if (info) addText(newBox, info);
    return newBox;
}

// Add attr logo to resource
function addResAttr(node, attr, img, title) {
    if (attr === false) return;

    const newNode = document.createElement("img");
    newNode.setAttribute("src", img);
    newNode.setAttribute("title", title);
    newNode.classList.add("res_attr");
    node.appendChild(newNode);
    return newNode;
}

// Change node class to resources_display and displays every resource and info requested, with separations between types
function displayResources(node, resCat, resInfo, resources) {
    if (!resInfo || !resources) return; // If no information for resource display or nothing to display, abort

    // If no categories provided
    if (resCat.length === 0) {
        resCat = [getText("default_resources_cat")];
        for (const [r, _] of Object.entries(resInfo)) resInfo[r].category = getText("default_resources_cat");
    }


    // Create nodes for each categories
    const resNodes = {};
    for (const cat of resCat) {
        const newDiv = document.createElement("div");
        node.appendChild(newDiv);
        
        // Add div & title
        const textDiv = document.createElement("div");
        const text = addText(textDiv, cat);
        text.classList.add("category_name");
        newDiv.appendChild(textDiv);
        
        const resDiv = document.createElement("div");
        resDiv.classList.add("resources_display");
        newDiv.appendChild(resDiv);

        resNodes[cat] = resDiv;
    }

    // Add every resource to corresponding category
    for (const [r, nb] of Object.entries(resources)) {
        if (!resNodes[resInfo[r].category]) continue;  // Skip if no category
        
        let newBox;
        if (r === "housing") {
            newBox = displayResource(r, resInfo[r].image_path, "");
            // Add and customize progress bar for this display
            const ratio = resources["housing"] > 0 ? resources["housed employee"] / resources["housing"] : 0;
            const bar = addProgressBar(newBox, ratio, `${resources["housed employee"]} / ${resources["housing"]}`);
            bar.classList.add("housing");
            bar.lastChild.classList.add("housing_text");
            bar.lastChild.setAttribute("title", getText("housing_progress_bar_help"));
        } else {
            newBox = displayResource(r, resInfo[r].image_path, (nb === undefined) ? "0" : nb.toString());
        }


        // Add logos for resources attributes
        addResAttr(newBox, resInfo[r].shared, "./images/logo_shared.svg", getText("res_shared_help"));
        addResAttr(newBox, resInfo[r].traded_broken, "./images/logo_traded_broken.svg", getText("res_traded_broken_help"));
        addResAttr(newBox, resInfo[r].reset, "./images/logo_reset.svg", getText("res_reset_help"));
        
        resNodes[resInfo[r].category].appendChild(newBox);
    }
}

// Change node class to resources_display and displays every resource with positive value
function displayPositiveResources(node, resInfo, resources) {
    if (!resInfo || !resources) return; // If no information for resource display or nothing to display, abort

    node.classList.add("resources_display");
    for (const [r, info] of Object.entries(resources)) {
        // Add every resource
        if (info === undefined || info === 0) continue;
        const newBox = displayResource(r, resInfo[r].image_path, info.toString());
        node.appendChild(newBox);
    }
}

// Display resources in the case of trades (with buttons to increment and decrement
// Changes node class to resources_display
function displayResourcesTrades(node, isCurrentPlayer, playerRes, resInfo, trade, playerBoolean) {
    if (!resInfo) return; // If no information for resource display, abort

    const newNode = document.createElement("div");
    newNode.classList.add("resources_display_trades");

    // -- Show name, status & button
    const nameAndStatus = document.createElement("div");
    nameAndStatus.classList.add("trade_name_and_status");
    newNode.appendChild(nameAndStatus);

    // Name
    addText(nameAndStatus, isCurrentPlayer ? getText("you") : trade.players[playerBoolean]);
    // Status image
    const img = document.createElement("img");
    img.setAttribute("src", trade.status[playerBoolean] === "OK" ? "./images/button_ok.svg" : "./images/button_pending.svg");
    img.setAttribute("title", getText(trade.status[playerBoolean] === "OK" ? "offer_ok_help" : "offer_pending_help"));
    img.classList.add("help_cursor");
    nameAndStatus.appendChild(img);

    if (isCurrentPlayer) {
        if (trade.status[playerBoolean] === "OK") addButton(nameAndStatus, getText("edit_trade_offer"), `setTradeState(${trade.id},"PENDING")`);
        else addButton(nameAndStatus, getText("accept_trade"), `setTradeState(${trade.id},"OK")`);
    }

    // Put resources in different arrays of an object (one for each category, order is order of resources)
    const obj = {};

    for (const [r, info] of Object.entries(trade.resources[playerBoolean])) {
        const cat = resInfo[r].category;
        if (!obj[cat]) obj[cat] = [];
        obj[cat].push([r, info]);
    }
    
    // For each category
    for (const [_, resources] of Object.entries(obj)) {
        const newCat = document.createElement("div");
        newCat.classList.add("resources_cat_trades");
        // Add every resource of given category
        for (const [r, info] of resources) {
            const newDiv = document.createElement("div");
            
            const resImgNode = displayResource(r, resInfo[r].image_path, "");
            if (playerRes[r] === 0) resImgNode.classList.add("darken"); // Darken image if player doesn't have it
            newDiv.appendChild(resImgNode);

            // Div for buttons and quantity display
            const newBox = document.createElement("div");
            newBox.classList.add("res_qt_modifier");

            const minusButton = addButton(newBox, "-", `updateTradeResource(${trade.id}, ${playerBoolean}, "${r}", ${info-1})`);
            if (info === 0) minusButton.classList.add("dark_button");

            const inputNode = addInput(newBox, "", info);
            inputNode.classList.add("res_qt");
            inputNode.addEventListener('change', function() {  
                const value = parseInt(inputNode.value);
                updateTradeResource(trade.id, playerBoolean, r, value ? value : 0);
            });

            const plusButton = addButton(newBox, "+", `updateTradeResource(${trade.id}, ${playerBoolean}, "${r}", ${info+1})`);
            if (info >= playerRes[r]) plusButton.classList.add("dark_button");
            
            newDiv.appendChild(newBox);
            newCat.appendChild(newDiv);
        }
        newNode.appendChild(newCat);
    }
    node.appendChild(newNode);
}

// Change node class to resources_display and displays every resource and info requested for recipe
function displayResourcesRecipe(node, resInfo, resources) {
    if (!resInfo || !resources) return; // If no information for resource display or nothing to display, abort

    node.classList.add("resources_display");
    for (let [r, nb] of Object.entries(resources)) {
        // Add every resource
        if (nb === undefined || nb === 0) nb = "";
        const newBox = displayResource(r, resInfo[r].image_path, nb.toString());
        node.appendChild(newBox);
    }
}

// Display full recipe for an activity
function displayRecipe(node, staticConfig, key, info) {
    const newNode = document.createElement("div");
    newNode.classList.add("display_recipe");

    // Display activity and image
    addTextH(newNode, key, 3);
    if (key in staticConfig.actInfo) {
        const img = document.createElement("img");
        img.setAttribute("src", staticConfig.actInfo[key].image_path);
        img.style.backgroundColor = staticConfig.actInfo[key].color;
        img.style.width = "min(5vw, 100px)";
        img.style.height = "min(5vw, 100px)";
        const shape = activityShape(info.terrains_req);
        img.style.padding = `min(${5 * (shape[1]-1) / 2}vw, ${100 * (shape[1]-1) / 2}px) min(${5 * (shape[0]-1) / 2}vw, ${100 * (shape[0]-1) /2}px)`;
        // Padding depending on activity size
        newNode.appendChild(img);
    }
    
    // Terrains cost
    if (info.terrains_req) {
        const terrainsDiv = document.createElement("div");
        terrainsDiv.classList.add("recipe_line");
        newNode.appendChild(terrainsDiv);
        addText(terrainsDiv, getText("terrains_recipe") + " " + info.terrains_req);
    }

    // Actions cost (only display if not default value of 1)
    if (info.actions_cost !== undefined && info.actions_cost !== 1) {
        const actionsDiv = document.createElement("div");
        addText(actionsDiv, `${getText("actions_cost_recipe")} ${info.actions_cost}`);
        newNode.appendChild(actionsDiv);
    }

    // Max number (only display if not default value of 0)
    if (info.nb_max) {
        const actionsDiv = document.createElement("div");
        addText(actionsDiv, `${getText("nb_max_recipe")} ${info.nb_max}`);
        newNode.appendChild(actionsDiv);
    }

    // Ration convertion
    if (info.turn_req["ration"]) {
        const rationDiv = document.createElement("div");
        addText(rationDiv, getText("convert_food_recipe"));
        // Add each possibility to get ration from other resources
        for (const [res, resInfo] of Object.entries(staticConfig.resInfo)) {
            if (resInfo.ration_value > 0) {
                const div = document.createElement("div");
                div.classList.add("ration_display");
                div.appendChild(displayResource(res, staticConfig.resInfo[res].image_path, 1));
                addText(div, "=");
                div.appendChild(displayResource("ration", staticConfig.resInfo["ration"].image_path, resInfo.ration_value));
                rationDiv.append(div);
            }
        }
        addText(rationDiv, getText("convert_food_tuto_recipe"));
        newNode.appendChild(rationDiv);
    }

    // Transform part
    const transfo = document.createElement("div");
    transfo.classList.add("transfo");

    // Req and prod info
    const nodeReq = document.createElement("div");
    displayResourcesRecipe(nodeReq, staticConfig.resInfo, info.turn_req);
    nodeReq.classList.add("recipe_res_display");
    transfo.appendChild(nodeReq);

    // Transformation arrow and employees
    const nodeMiddle = document.createElement("div");

    // Employees (Display if any)
    if (info.turn_prod["employee"]) {
        nodeMiddle.appendChild(displayResource("employee", staticConfig.resInfo["employee"].image_path, info.turn_prod["employee"]));
        delete info.turn_prod["employee"];
    }

    // Transformation arrow
    const arrowImg = document.createElement("img");
    arrowImg.setAttribute("src", "./images/right_arrow.svg");
    nodeMiddle.appendChild(arrowImg);

    transfo.appendChild(nodeMiddle);

    const nodeProd = document.createElement("div");
    displayResourcesRecipe(nodeProd, staticConfig.resInfo, info.turn_prod);
    nodeProd.classList.add("recipe_res_display");
    transfo.appendChild(nodeProd);

    newNode.appendChild(transfo);

    node.appendChild(newNode);
    return newNode;
}

// Return displayed shape, closest to square
function activityShape(nb_terrains) {
    if (!nb_terrains) return [1, 1];

    let res = [nb_terrains, 1];
    let i = 2;
    while (i*i <= nb_terrains) {
        if (!(nb_terrains%i)) {
            res = [nb_terrains / i, i];
            }
        i += 1;
    }
    return res;
}

// Display resources cost in line (for road repair)
function displayResourcesCost(node, resInfo, resources) {
    if (!resInfo || !resources) return; // If no information for resource display or nothing to display, abort

    const resourcesNode = document.createElement("div");
    resourcesNode.classList.add("resources_road");
    resourcesNode.classList.add("recipe_res_display");

    // Add every resource
    for (const [r, nb] of Object.entries(resources)) {
        resourcesNode.appendChild(displayResource(r, resInfo[r].image_path, (nb === undefined) ? "0" : nb.toString()));
    }

    node.appendChild(resourcesNode);
}

// Display housing menu
function displayHousing(node, staticConfig, nbHoused, maxHoused) {
    addText(node, getText("nb_employees_housed"));

    // Div for quantity display
    const resDivQt = document.createElement("div");

    const minusButton = addButton(resDivQt, "-", `updateHoused(${nbHoused-1})`);
    if (nbHoused === 0) minusButton.classList.add("dark_button");

    const inputNode = addInput(resDivQt, "", nbHoused);
    inputNode.classList.add("res_qt");
    inputNode.addEventListener('change', function() {  
        const value = parseInt(inputNode.value);
        updateHoused(value ? value : 0);
    });

    const plusButton = addButton(resDivQt, "+", `updateHoused(${nbHoused+1})`);
    if (nbHoused >= maxHoused) plusButton.classList.add("dark_button");

    node.appendChild(resDivQt);

    addButton(node, getText("ok_button"), `sio.emit("house", "${staticConfig.playerName}", ${nbHoused});closePopup();`);
}

// Display resources to convert to rations
function displayResRation(node, staticConfig, playerRes, conv) {
    const content = document.createElement("div");
    content.classList.add("resources_display_trades");

    for (const [res, info] of Object.entries(staticConfig.resInfo)) {
        if (info.ration_value === 0) continue;

        if (!conv[res]) conv[res] = 0; // First time opening popup

        // Div for given resource
        const resDiv = document.createElement("div");

        // First sub-div is image
        resDiv.appendChild(displayResource(res, staticConfig.resInfo[res].image_path, ""));
        
        // Second sub-div is for quantity display
        const resDivQt = document.createElement("div");

        const minusButton = addButton(resDivQt, "-", `updateConvertResource("${res}", ${conv[res]-1})`);
        if (conv[res] === 0) minusButton.classList.add("dark_button");

        const inputNode = addInput(resDivQt, "", conv[res]);
        inputNode.classList.add("res_qt");
        inputNode.addEventListener('change', function() {  
            const value = parseInt(inputNode.value);
            updateConvertResource(res, value ? value : 0);
        });

        const plusButton = addButton(resDivQt, "+", `updateConvertResource("${res}", ${conv[res]+1})`);
        if (conv[res] >= playerRes[res]) plusButton.classList.add("dark_button");

        resDiv.appendChild(resDivQt);

        // Last sub-div is for validation button
        addButton(resDiv, `${getText("click_to_convert")}\n(1 ${res} = ${info.ration_value} ration(s))`, `sio.emit("convert_ration", "${staticConfig.playerName}", "${res}", ${conv[res]});updateConvertResource("${res}", 0);`);
        
        content.appendChild(resDiv);
    }
    node.appendChild(content);
}