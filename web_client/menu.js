const ip = window.location.host;
var sio = io.connect(ip);

function infoTxt(msg) {
    document.getElementById("info_txt").textContent = msg;
}

/* RENAME PLAYER AND JOIN GAME */

function renamePlayer(id) {
    const node = document.getElementById("box_p" + id).firstChild;
    const oldName = node.defaultValue;
    const newName = node.value;
    // Rename only if necessary
    if (oldName !== newName) {
        sio.emit("rename_player", oldName, newName);
    }
}

function joinGame(id) {
    const name = document.getElementById("box_p" + id).firstChild.defaultValue;
    let gameUrl = "game.html?player_name=" + name;
    const lang = new URLSearchParams(window.location.search).get("lang");
    if (lang) gameUrl += "&lang=" + lang;
    document.location = gameUrl;
}

window.onload = function() {
    loadTexts();

    let adminUrl = "./admin.html";
    const lang = new URLSearchParams(window.location.search).get("lang");
    if (lang) adminUrl += "?lang=" + lang;
    document.getElementById("admin_page_link").setAttribute("href", adminUrl);

    sio.emit("get_players_names");
};

/* SOCKET INTERACTIONS */

sio.on("log", (msg) => {
    infoTxt(msg);
});

sio.on("get_players_names", (players) => {
    infoTxt(getText("connection_ok"));

    // Reset and display players list
    const node = cleanNode("players");

    let counter = 0;

    players.forEach(element => {
        // Put elements for every player
        const newBox = document.createElement("div");
        newBox.setAttribute("id", "box_p" + counter);
        addInput(newBox, "", element);
        addButton(newBox, getText("rename_button"), "renamePlayer(" + counter + ")");
        addButton(newBox, getText("join_button"), "joinGame(" + counter + ")");

        node.appendChild(newBox);

        counter++;
    });
});
