// Setup game canvas and global functions
// Game behaviours are NOT described here (but in board and other files)

/* Creates canvas used for board display */
function initCanvas() {
    const container = document.getElementById("terrains_cell");
    
    // Create the Konva stage
    const margin = parseInt(window.getComputedStyle(document.getElementById("terrains_cell")).margin);
    const width = document.getElementById("col_2").offsetWidth - 2*margin;
    var stage = new Konva.Stage({
        container: "terrains_cell",
        width: width,
        height: container.offsetHeight,
        draggable: false,
    });
    
    // Add layer to stage
    var movingLayer = new Konva.Layer({"draggable": true});
    stage.add(movingLayer);
    
    window.addEventListener("resize", function() {resizeCanvas(stage, 0);});

    var background = new Konva.Rect({
        x: -1800,
        y: -1800,
        width: 3600,
        height: 3600,
        fill: "green"
    })
    movingLayer.add(background);

    // Beginning offset
    movingLayer.position({x: stage.width()/2, y: stage.height()/2});


    // Add HUD layer to stage (not draggable);
    var hud = new Konva.Layer();
    stage.add(hud);

    var buttonPlusImg = new Image();
    buttonPlusImg.src = "./images/button_plus.svg";
    var buttonPlus = new Konva.Image({x: 14, y: 14, width: 40, height: 40, image: buttonPlusImg});
    clickable(buttonPlus);
    hud.add(buttonPlus);
    
    var buttonMinusImg = new Image();
    buttonMinusImg.src = "./images/button_minus.svg";
    var buttonMinus = new Konva.Image({x: 14, y: 14*2+40, width: 40, height: 40, image: buttonMinusImg});
    clickable(buttonMinus);
    hud.add(buttonMinus);

    // Setup buttons interactivity and moving the scene
    setupSceneMove(stage, movingLayer, buttonPlus, buttonMinus);

    return stage;
}

/* Function called when canvas is resized to adapt Konva stage */
function resizeCanvas(stage, xOffset) {
    const layer = stage.getLayers()[0];
    const margin = parseInt(window.getComputedStyle(document.getElementById("terrains_cell")).margin);
    
    stage.height(document.getElementById("terrains_cell").offsetHeight);
    stage.width(document.getElementById("col_2").offsetWidth - 2*margin);
    refreshPosition(layer, xOffset);
}

/* Change cursor when over element */
function clickable(elem) {
    elem.on("mouseover", function () {
        document.body.style.cursor = "pointer";
    });
    elem.on("mouseout", function () {
        document.body.style.cursor = "default";
    });
}
