const MINZOOM = 0.2;
const MAXZOOM = 1;

/* Drag has limits so that player doesn't get lost while moving around
The board is constrained to stay in at least half the width and half the height of the screen */
function refreshPosition(stage, xOffset) {
    var newPos = {
        x: stage.x() + xOffset,  // xOffset is used when player moves columns
        y: stage.y(),
    };

    const boardDimensions = getBoardValues();

    const minPos = {
        x: -boardDimensions.width/2 * stage.scaleX() + stage.width()/2,
        y: -boardDimensions.height/2 * stage.scaleX() + stage.height()/2
    };

    const maxPos = {
        x: boardDimensions.width/2 * stage.scaleX() + stage.width()/2,
        y: boardDimensions.height/2 * stage.scaleX() + stage.height()/2
    };

    newPos.x = Math.max(minPos.x, Math.min(maxPos.x, newPos.x));
    newPos.y = Math.max(minPos.y, Math.min(maxPos.y, newPos.y));

    stage.position(newPos);
}

/* Setup scene interaction with user for zoom and move (stage is reference stage, layer contain the elements that should react to this zoom)
- Move is done by dragging and is limited by the scene's dimensions
- Zoom is done with mouse wheel or buttons through a layer called HUD
*/
function setupSceneMove(stage, layer, buttonPlus, buttonMinus) {
    var scaleBy = 1.05;

    function zoom(intensity, zoomAtCenter) {
        var oldScale = layer.scaleX();
        var pointer;
        
        if (zoomAtCenter) pointer = {x: layer.width()/2, y: layer.height()/2};
        else pointer = stage.getPointerPosition();


        var mousePointTo = {
            x: (pointer.x - layer.x()) / oldScale,
            y: (pointer.y - layer.y()) / oldScale,
        };
        
        var newScale = oldScale * Math.pow(scaleBy, intensity);
        // Limit scale size
        newScale = Math.min(MAXZOOM, Math.max(MINZOOM, newScale));

        layer.scale({ x: newScale, y: newScale });

        var newPos = {
            x: pointer.x - mousePointTo.x * newScale,
            y: pointer.y - mousePointTo.y * newScale,
        };

        layer.position(newPos);
        
        refreshPosition(layer, 0);
    }
    
    layer.on("dragmove", (e) => {
        refreshPosition(layer, 0);
    });

    // Original code is from this Konva example:
    // https://konvajs.org/docs/sandbox/Zooming_Relative_To_Pointer.html
    layer.on("wheel", (e) => {
        // stop default scrolling
        e.evt.preventDefault();

        // how to scale? Zoom in? Or zoom out?
        const direction = e.evt.deltaY > 0 ? -1 : 1;

        zoom(direction, false);
    });

    buttonPlus.on("click", (_) => {
        zoom(5, true);
    });

    buttonMinus.on("click", (_) => {
        zoom(-5, true);
    });
}
