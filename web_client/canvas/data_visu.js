var visu = {};     // Konva groups for trades visualizations

function placeVisu(staticConfig) {
    // Create groups for trade visualization
    for (const player of staticConfig.playersList) {

        const grp = new Konva.Group({});
        grp.rotate(getAngles()[player]);

        // Add group
        staticConfig.konvaBoard.getLayers()[0].add(grp);
        visu[player] = grp;
    }
}

function updateVisu(staticConfig, data) {
    for (const [playerName, grp] of Object.entries(visu)) {
        grp.destroyChildren();

        if (!data[playerName]) continue; // Skip if player has no data

        const playerData = data[playerName];

        // For each resource, add arrow and image
        let yPosImport = 100, yPosExport = -100;
        for (const [res, val] of Object.entries(playerData)) {
            if (val === 0) continue;

            const resGrp = new Konva.Group({});

            let points, yPos;
            if (val < 0) {  // Import
                points = [-100, 0, 150, 0];
                yPos = yPosImport;
                yPosImport += 60;
            } else {  // Export
                points = [150, 0, -100, 0];
                yPos = yPosExport;
                yPosExport -= 60;
            }

            // Log scale for arrow size
            const arrow = new Konva.Arrow({
                points: points,
                pointerLength: 15,
                pointerWidth: 20,
                fill: staticConfig.resInfo[res].color,
                stroke: staticConfig.resInfo[res].color,
                strokeWidth: 10*Math.log2(Math.abs(val)+1),
                offset: {
                    x: -650,
                    y: yPos
                }
            });
            resGrp.add(arrow);

            // Resource image
            const resImg = new Image();
            resImg.src = staticConfig.resInfo[res].image_path;
            const r = new Konva.Image({
                x: 500,
                y: -yPos,
                offset: {
                    x: 30, y: 30,
                },
                image: resImg,
                width: 60,
                height: 60,
            });
            r.rotate(-getAngles()[playerName]);
            resGrp.add(r);

            // Text
            const t = new Konva.Text({
                x: 450,
                y: -yPos,
                offset: {
                    x: 30, y: 12.5,
                },
                width: 60,
                text: Math.abs(val),
                fontSize: 25,
                fill: "white",
                align: "center",
            });
            t.rotate(-getAngles()[playerName]);
            resGrp.add(t);

            // Show detail if hover
            clickable(resGrp);
            resGrp.on("click", () => {
                const title = [getText("flows_of"), res, getText("for"), playerName];
                const popupNode = openPopup(title.join(" "));
                const img = document.createElement("img");
                img.setAttribute("src", `./visu/${playerName}_${res}.svg?${performance.now()}`);  // Force image fetch with time as argument
                img.classList.add("visu_render");
                popupNode.appendChild(img);
            });

            grp.add(resGrp);
        }
    }
}
