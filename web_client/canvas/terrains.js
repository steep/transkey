var players = {};  // Konva groups for players and terrains

/* PLAYERS SETUP */

function placePlayers(staticConfig) {
    // Every player is an object containing two groups:
    // - res: Resources
    // - terrains: Terrains
    for (const playerName of staticConfig.playersList) {
        players[playerName] = {};

        const group = new Konva.Group();
        const angle = getAngles()[playerName] * Math.PI / 180;
        group.position({x: Math.cos(angle) * 1300, y: Math.sin(angle) * 1300});

        const outline = new Konva.Rect({
            x: 0,
            y: 0,
            width: 700,
            height: 700,
            fill: "green",
            stroke: "black",
            strokeWidth: 10,
            offset: {
                x: 350,
                y: 350,
            }
        });
        group.add(outline);

        const playerNameTxt = new Konva.Text({
            x: -350,
            y: -330,
            width: 700,
            fontSize: 60,
            fontStyle: "bold",
            fill: "white",
            text: playerName,
            align: "center",
        });
        group.add(playerNameTxt);

        // Trade outline & name
        if (playerName != staticConfig.playerName) {
            const tradeGroup = new Konva.Group({});

            const tradeOutline = new Konva.Rect({
                x: 0,
                y: -410,
                width: 350,
                height: 60,
                fill: "lightgray",
                stroke: "black",
                strokeWidth: 5,
                offset: {
                    x: 175,
                    y: 30,
                }
            });
            tradeGroup.add(tradeOutline);

            const tradeText = new Konva.Text({
                x: -350,
                y: -420,
                width: 700,
                fontSize: 25,
                fontStyle: "bold",
                fill: "black",
                text: getText("begin_new_trade_button"),
                align: "center",
            });
            tradeGroup.add(tradeText);

            clickable(tradeGroup);
            tradeGroup.on("click", () => {
                newTrade(playerName);
            });
            group.add(tradeGroup);
        }

        const resGroup = new Konva.Group({});
        group.add(resGroup);
        players[playerName].res = resGroup;

        const terrainsGroup = new Konva.Group({});
        group.add(terrainsGroup);
        players[playerName].terrains = terrainsGroup;
        
        staticConfig.konvaBoard.getLayers()[0].add(group);
        
    }
}

/* TERRAINS */

const dashImg = () => {const tmp = new Image(); tmp.src = "./images/terrain_overlay_dash.svg"; return tmp};

// Update terrains display for player
function updateTerrains(staticConfig, playerName, terrains, totalSize) {
    players[playerName].terrains.destroyChildren();

    const actSize = 100;  // nb of pixels of a cell width
    const offset = {x:250, y:100};
    
    // Place dotted lines
    // Create whole area with empty terrains
    for (let w=0; w<totalSize[0]; w++) {
        for (let h=0; h<totalSize[1]; h++) {
        players[playerName].terrains.add(new Konva.Image({position: {x: actSize*w, y: actSize*h},
                                                          offset: offset,
                                                          size: {x: actSize, y: actSize},
                                                          image: dashImg()}));
        }
    }

    // Place activities
    terrains.forEach(function(terrain, id) {

        const activity = terrain.act;
        const size = {width: actSize*terrain.size[0], height: actSize*terrain.size[1]};
        const pos = {x: actSize*terrain.pos[0], y: actSize*terrain.pos[1]};

        const grp = new Konva.Group({offset: offset});

        // BG color
        const col = staticConfig.actInfo[activity.name].color;
        const bgCol = new Konva.Rect({position: pos, size: size, fill: col, strokeWidth: 4, stroke: 'black'});
        grp.add(bgCol);

        // Activity size proportional to number of terrains it takes
        // Display activity
        const actImg = new Image();
        actImg.src = staticConfig.actInfo[activity.name].image_path;
        const act = new Konva.Image({position: {x: (terrain.pos[0] + (terrain.size[0] - 1)/2) * actSize, y: (terrain.pos[1] + (terrain.size[1] - 1)/2) * actSize},
                                     size: {width: actSize, height: actSize},
                                     image: actImg});
        grp.add(act);

        // Activity off overlay (darken if not running)
        if (!activity.running) {
            grp.add(new Konva.Rect({position: pos, size: size, fill: "rgba(0, 0, 0, 0.6)"}));
        }

        // Player can interact with activities
        grp.on("click", () => {
            activityInteraction(staticConfig, playerName, id, activity);
        });
        clickable(grp);
        players[playerName].terrains.add(grp);
    });

}

/* DISPLAY PLAYERS RESOURCES */

// Displays resources on position requested
function canvasDisplayResources(node, pos, resInfo, resources) {
    node.destroyChildren();
    for(const [resName, val] of Object.entries(resources)) {
        // Skip if should not be shown
        if (!resInfo[resName].visible) continue;

        // Resource image
        const resImg = new Image();
        resImg.src = resInfo[resName].image_path;
        const r = new Konva.Image({
            x: pos.x,
            y: pos.y,
            image: resImg,
            width: 60,
            height: 60,
        });
        node.add(r);

        const t = new Konva.Text({
            x: pos.x,
            y: pos.y + 70,
            width: 60,
            text: val,
            fontSize: Math.min(20+val, 50),
            fill: "white",
            align: "center",
        });
        node.add(t);
        pos.x += 55;
    }
}

// Updates resources for given player
function updateResources(playerName, resInfo, resources) {
    canvasDisplayResources(players[playerName].res, {x: -330, y: -250}, resInfo, resources);
    showGHG(resources["greenhouse gas"], resInfo["greenhouse gas"]);
}

/* CENTER */

var ghgText;  // Konva text for greenhouse gas
var ghgImg;  // Konva text for greenhouse gas

function placeGHG(staticConfig) {
    const ghgGroup = new Konva.Group({});

    // Resource image
    ghgImg = new Image();
    const ghgKonvaImage = new Konva.Image({
        x: -100,
        y: -120,
        image: ghgImg,
        width: 200,
        height: 200,
    });
    ghgGroup.add(ghgKonvaImage);

    ghgText = new Konva.Text({
        x: -200,
        y: 80,
        width: 400,
        fontSize: 50,
        fontStyle: "bold",
        fill: "white",
        align: "center",
    });
    ghgGroup.add(ghgText);

    staticConfig.konvaBoard.getLayers()[0].add(ghgGroup);
}

function showGHG(ghg, ghgInfo) {
    if (ghg === undefined) return;
    ghgText.text(ghg);
    ghgText.fontSize(50 + ghg);
    ghgText.fill(`rgb(255, ${255-4*ghg}, ${255-4*ghg})`);

    if (ghgInfo) ghgImg.src = ghgInfo.image_path;
}
