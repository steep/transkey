var roads = {};    // Konva groups for roads

/* ROADS */

function placeRoads(staticConfig) {
    // Load and place roads for given number of players
    for (const player of staticConfig.playersList) {
        const grp = new Konva.Group();

        // Add group
        staticConfig.konvaBoard.getLayers()[0].add(grp);
        roads[player] = grp;
    }
}

// Function has to be called after a fixRoad for specified player
function breakRoad(staticConfig, playerName, brokenRoad) {
    const road = roads[brokenRoad].children[0];

    const roadImg = new Image();
    roadImg.src = "./images/road_broken.svg";
    road.image(roadImg);

    const angle = getAngles()[brokenRoad] * Math.PI / 180;
    
    const fixText = new Konva.Text({
        position: {x: Math.cos(angle) * 650, y: Math.sin(angle) * 650},
        offset: {x: 90, y: 25},
        width: 180,
        text: getText("open_repair_road_window_canvas"),
        fontSize: 25,
        fill: "white",
        align: "center",
    });
    roads[brokenRoad].add(fixText);

    roads[brokenRoad].on("click", () => {
        // Open popup to repair and display recipe
        const node = openPopup(getText("repair_road_popup") + " " + brokenRoad);
        displayResourcesCost(node, staticConfig.resInfo, staticConfig.fixRoadCost);
        const but = addButton(node, getText("repair_road_button"), `sio.emit("fix_road", "${playerName}","${brokenRoad}");closePopup();`);
        // If player is missing resources when opening popup, turn button gray
        if (!hasEnoughRes(getPlayerRes(), staticConfig.fixRoadCost)) {
            but.classList.add("dark_button");
        }
    });
    clickable(roads[brokenRoad]);
}

function fixRoad(brokenRoad) {

    roads[brokenRoad].destroyChildren();
    roads[brokenRoad].off("click"); // Turn off function used when clicking on road
    roads[brokenRoad].off("mouseover"); // Turn off cursor change when hovering road

    const roadImg = new Image();
    roadImg.src = "./images/road.svg";

    const road = new Konva.Image({
        x: 0,
        y: 0,
        width: 700,
        height: 100,
        offset: {
            x: -300,
            y: 50,
        }
    });
    road.image(roadImg);
    road.rotate(getAngles()[brokenRoad]);
    road.off("click");
    
    roads[brokenRoad].add(road);
}

function updateRoads(staticConfig, brokenL) {
    for (const p of staticConfig.playersList) {
        fixRoad(p);
        if (brokenL.includes(p)) breakRoad(staticConfig, staticConfig.playerName, p);
    }
}
