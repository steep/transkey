var board = {
    width: 3600,
    height: 3600
};
function getBoardValues() {return board;}

var angles = {};   // For each player, keep angle of rotation in terrains
function getAngles() {return angles;}

function setupBoard(staticConfig) {
    let angle = 0;
    for (const player of staticConfig.playersList) {
        angles[player] = angle;
        angle += 360 / (staticConfig.playersList.length);
    }

    // Add center road
    const roadImg = new Image();
    roadImg.src = "./images/road_circle.svg";
    const centerRoad = new Konva.Image({
        x: 0,
        y: 0,
        width: 700,
        height: 700,
        offset: {
            x: 350,
            y: 350,
        },
        image: roadImg,
    });
    centerRoad.rotate(1);
    staticConfig.konvaBoard.getLayers()[0].add(centerRoad);

    placeRoads(staticConfig);
    updateRoads(staticConfig, []);
    placePlayers(staticConfig);
    placeGHG(staticConfig);
    placeVisu(staticConfig);
}
