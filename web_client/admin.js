const ip = window.location.host;
var sio = io.connect(ip);

var res = new Set();

/* LOG */

function infoTxt(msg) {
    document.getElementById("info_txt").textContent = msg;
}

sio.on("log", (msg) => infoTxt(msg));

/* ADMIN COMMANDS */

function loadConfig() {
    const folder = document.getElementById("folder").value;
    sio.emit("load_config", folder);
}

function forceStart() {
    sio.emit("force_start");
    sio.emit("get_config");
}

window.onload = function() {
    loadTexts();

    let gameUrl = "./game.html?admin_view=true";
    const lang = new URLSearchParams(window.location.search).get("lang");
    if (lang) gameUrl += "&lang=" + lang;
    document.getElementById("game_page_link").setAttribute("href", gameUrl);

    sio.emit("get_config");
};

/* SOCKET INTERACTIONS */

sio.on("get_players_names", (_) => infoTxt(getText("log_load_ok")));

sio.on("game_start", () => infoTxt(getText("log_game_start")));

var players = {};

sio.on("get_config", (playersList, _) => {
    players = playersList;

    const node = cleanNode("admin_buttons");

    addTextH(node, getText("admin_next_buttons"), 3);

    addButton(node, getText("admin_force_next_phase"), `sio.emit("admin_force_next_phase")`);
    addText(node, "");
    addButton(node, getText("admin_force_next_turn_visu"), `sio.emit("admin_force_next_turn")`);
    addText(node, "");
    addButton(node, getText("admin_force_next_turn_no_visu"), `sio.emit("admin_force_next_turn",true)`);

    // Add buttons to break and repair roads
    addTextH(node, getText("admin_break_road"), 3);
    for (const player of players) {
        addButton(node, player, `sio.emit("admin_break_road","${player}")`);
    }

    addTextH(node, getText("admin_repair_road"), 3);
    for (const player of players) {
        addButton(node, player, `sio.emit("admin_fix_road","${player}")`);
    }
    sio.emit("get_info", players[0]);
});

sio.on("get_activities_info", (actInfo) => {
    actInfo = JSON.parse(actInfo);
    
    // Add buttons for adding activities
    const add = cleanNode("add");

    // Select player
    const selectPlayer = document.createElement("select");
    selectPlayer.setAttribute("id", "select_player_1");
    add.appendChild(selectPlayer);

    for (const player of players) {
        addOption(selectPlayer, player);
    }

    // Select activity
    const selectAct = document.createElement("select");
    selectAct.setAttribute("id", "select_act_1");
    add.appendChild(selectAct);

    for (const [key, _] of Object.entries(actInfo)) {
        addOption(selectAct, key);
    }
    
    addButton(add, getText("admin_add_act_button"), "force_add_activity()");

    // Add buttons for removing activities
    const rem = cleanNode("rem");

    // Select player
    const selectPlayer2 = document.createElement("select");
    selectPlayer2.setAttribute("id", "select_player_2");
    rem.appendChild(selectPlayer2);

    for (const player of players) {
        addOption(selectPlayer2, player);
    }

    // Select activity
    const selectAct2 = document.createElement("select");
    selectAct2.setAttribute("id", "select_act_2");
    rem.appendChild(selectAct2);

    for (const [key, _] of Object.entries(actInfo)) {
        addOption(selectAct2, key);
    }
    
    addButton(rem, getText("admin_rm_act_button"), "force_remove_activity()");
});

function force_add_activity() {
    sio.emit("admin_force_add_activity",
            `${document.getElementById("select_player_1").value}`,
            `${document.getElementById("select_act_1").value}`);
}

function force_remove_activity() {
    sio.emit("admin_force_remove_activity",
        `${document.getElementById("select_player_2").value}`,
        `${document.getElementById("select_act_2").value}`);
}

sio.on("get_resources_info", (_, resInfo) => {
    resInfo = JSON.parse(resInfo);

    const node = cleanNode("admin_buttons_res");

    // Add buttons for adding activities
    addTextH(node, getText("admin_give_res"), 3);

    // Select player
    const selectPlayer = document.createElement("select");
    selectPlayer.setAttribute("id", "select_player_3");
    node.appendChild(selectPlayer);

    for (const player of players) {
        addOption(selectPlayer, player);
    }

    // Select res
    const selectRes = document.createElement("select");
    selectRes.setAttribute("id", "select_res");
    node.appendChild(selectRes);

    for (const [key, _] of Object.entries(resInfo)) {
        addOption(selectRes, key);
        res.add(key);
    }

    addInput(node, "res_qt", 0);
    addButton(node, getText("admin_give_res_button"), "give_res()");
});

function give_res() {
    const dict = {};
    dict[document.getElementById("select_res").value] = parseInt(document.getElementById("res_qt").value);
    sio.emit("admin_give_res", `${document.getElementById("select_player_3").value}`, dict);
}

// Update recipes
sio.on("get_activities_info", (resInfo) => {
    resInfo = JSON.parse(resInfo);

    const node = cleanNode("admin_buttons_recipe");

    // Add buttons for adding activities
    addTextH(node, getText("admin_update_recipe"), 3);

    // Select player
    const selectPlayer = document.createElement("select");
    selectPlayer.setAttribute("id", "select_player_4");
    node.appendChild(selectPlayer);

    for (const player of players) {
        addOption(selectPlayer, player);
    }

    // Select recipe
    const selectRecipe = document.createElement("select");
    selectRecipe.setAttribute("id", "select_recipe");
    node.appendChild(selectRecipe);

    for (const [key, _] of Object.entries(resInfo)) {
        addOption(selectRecipe, key);
    }

    // Select res
    const selectRes = document.createElement("select");
    selectRes.setAttribute("id", "select_res_2");

    for (const key of res.values()) {
        addOption(selectRes, key);
    }

    node.appendChild(selectRes);

    addInput(node, "res_qt_2", 0);
    addButton(node, getText("admin_update_recipe_button"), "update_recipe()");
});

function update_recipe() {
    const dict = {};
    dict[document.getElementById("select_res_2").value] = parseInt(document.getElementById("res_qt_2").value);

    sio.emit("admin_change_recipe", `${document.getElementById("select_player_4").value}`,
                                    `${document.getElementById("select_recipe").value}`,
                                    dict);
}
