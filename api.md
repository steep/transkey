Ce document présentes l'API pour le système de socket et aussi de requêtes web (REST)

# Définition de l'API (Socket)

### Mise en place d'une partie

**Chargement d'une config :** `load_config(folder_path)`
- `folder_path` : `str`, chemin de la config
- OK: appelle `get_config()`
- KO: `log`

**Récupération d'une config :** `get_config()`
- OK: broadcast de `get_config` contenant les informations nécessaires pour la partie
- KO: `log`

**Renommer un joueur :** `rename_player(old_name, new_name)`
- `old_name` : `str`, ancien nom du joueur
- `new_name` : `str`, nouveau nom du joueur
- OK: appelle `get_config(get_resources_info)`
- KO: `log`

**Démarrer la partie :** `force_start()`
- OK: broadcast `game_start()` pour que les clients déjà connectés sur la page de jeu demandent les informations.
- KO: `log`
___
### Tour en cours

**Valider/Invalider fin d'une phase pour le joueur**: `set_phase_state(player_name, state)`
- `player_name`: `str`, nom du joueur
- `state`: `bool` état souhaité (`True` si le joueur valide la phase, `False` s'il veut finalement continuer à modifier quelque chose).
  l'état passe à `False` si le joueur effectue une autre action. Si tous les joueurs sont `True`, le jeu passe à la phase suivante.
- OK:
  - Si passage à la phase suivante, emit `next_phase` avec le numéro de phase
  - Sinon : appel de getters (`get_phase_state`, `get_nb_players_done`)
- KO: `log`
___
### Infos

**Tout :** `get_info(player_name)`
- `player_name`: `str`, nom du joueur
- OK: appel tous les getters (`get_resources`, `get_recipes`, ... qui emit des messages aux mêmes noms)
- KO: `log`

**Description du territoire :** `get_desc(player_name)`
- `player_name`: `str`, nom du joueur
- OK: `get_desc` contenant le texte de description du territoire du joueur associé
- KO: `log`

**Ressources :** `get_resources(player_name)`
- `player_name`: `str`, nom du joueur
- OK: `get_resources` contenant les ressources du joueur demandé
- KO: `log`

**Terrains :** `get_terrains(player_name)`
- `player_name`: `str`, nom du joueur
- OK: `get_terrains` contenant les terrains du joueur demandé
- KO: `log`

**Liste des noms de joueurs :** `get_players_names()`
- OK: `get_players_names` contenant un tableau des noms de joueurs
- KO: `log`
___
### Activités

**Placer une activité :** `add_activity(player_name, activity_name)`
- `player_name`: `str`, nom du joueur
- `activity_name`: `str`, nom de l'activité
- OK: appel de différents getters (`get_resources`, `get_terrains`)
- KO: `log`

**Allumer/Éteindre une activité :** `set_activity_state(player_name, terrain_id, state)`
- `player_name`: `str`, nom du joueur
- `terrain_id`: `int`, ID du terrain
- `state`: `str` ("on" ou "off"), état souhaité pour l'activité ("off" a été desactivé, ancienne mécanique)
- OK: appel de différents getters (`get_terrains`, `get_phase_state`, `get_nb_players_done`)
- KO: `log`

**Retirer une activité :** `remove_activity(player_name, terrain_id)`
- `player_name`: `str`, nom du joueur
- `terrain_id`: `int`, ID du terrain
- OK: appel de différents getters (`get_resources`, `get_terrains`, `get_phase_state`, `get_nb_players_done`, `get_nb_actions`)
- KO: `log`
___
### Echanges

Protocole d'échange de ressources :
- Un joueur peut démarrer un échange avec un autre (génère un ID unique pour cet échange) en indiquant quelles ressources il souhaite envoyer et recevoir. (NOTE : le client web part avec tout à 0).
- Les joueurs peuvent passer en attente de validation ou valider l'échange quand ils le souhaitent.
- Les joueurs peuvent modifier l'échange proposé (ressources ou nombres) avant de valider, cela refait passer les deux en attente de validation.
- Quand les deux joueurs valident, l'échange est accepté et a lieu
- Si un joueur décide de refuser l'échange, il est totalement annulé
- La validation d'un échange n'a lieu que si les joueurs ont assez de ressources.
- _Note: On considère que les ressources partagées ne sont pas échangées entre les joueurs_

**Demande d'échange :** `start_trade(player1_name, player2_name, player1_res, player2_res)`
- `player1_name` : `str`, nom du joueur qui soumet la demande
- `player2_name` : `str`, nom du joueur qui reçoit la demande
- `player1_res` : `dict`, ressources que le joueur propose dans l'échange
- `player2_res` : `dict`, ressources que le joueur demande dans l'échange
- OK: envoi de `info_trade` à tout le monde. Les joueurs concernés font la demande du getter `get_trades`
- KO: `log`

**Récupération des échanges :** `get_trades(player_name)`
- `player_name` : `str`, nom du joueur
- OK: envoi de `get_trades` avec un dictionnaire des échanges en cours pour le joueur
- KO: `log`

**Accepter / Refuser échange :** `set_trade_state(player_name, trade_id, state)`
- `player_name` : `str`, nom du joueur
- `trade_id` : `int`, ID unique de l'échange
- `state` : `OK`, `PENDING` ou `KO`
- OK: envoi de `info_trade` à tout le monde. Les joueurs concernés font la demande du getter `get_trades`
- KO: `log`

**Modifier l'échange :** `update_trade(trade_id, current_player, current_player_res, other_player_res)`
- `trade_id` : `int`, ID unique de l'échange
- `current_player` : `str`, nom du joueur
- `current_player_res` : `dict` contenant les ressources proposées
- `other_player_res` : `dict` content les ressources demandées à l'autre joueur
- OK: envoi de `info_trade` à tout le monde. Les joueurs concernés font la demande du getter `get_trades`
- KO: `log`

**Réparer une route :** `fix_road(current_player, isolated_player)`
- `current_player` : `str`, nom du joueur qui répare la route
- `isolated_player` : `str`, nom du joueur dont la route est cassée
- OK: envoi de `get_broken_roads` à tout le monde.
- KO: `log`
___
### Hébergement

**Convertir nourriture en rations** : `convert_ration(current_player, res, qt)`
- `current_player` : `str`, nom du joueur qui souhaite mettre à jour le nombre d'employés hébergés
- `res` : `str`, nom de la ressource à convertir
- `qt` : `int`, nombre d'unités à convertir
- OK: envoi de `get_resources` à tout le monde.
- KO: `log`

**Héberger:** `house(current_player, nb_housed)`
- `current_player` : `str`, nom du joueur qui souhaite mettre à jour le nombre d'employés hébergés
- `nb_housed` : `int`, nombre d'employés hébergés
- OK: envoi de `get_resources` à tout le monde.
- KO: `log`
___
### Admin
_Les commandes suivantes existent afin de faciliter le debug et le rôle d'admin lors des parties_

**Donner des ressources :** `admin_give_res(player, res_dict)`
- `player` : `str` nom du joueur qui perçoit les ressources
- `res_dict`: `dict` contenant les ressources générées
- OK: appel de `get_resources(player, True)`

**Forcer le tour suivant :** `admin_force_next_turn()`
- OK: `next_phase` à tout le monde

**Forcer la phase suivante :** `admin_force_next_phase()`
- OK: `next_phase` à tout le monde

**Casser une route :** `admin_break_road(player)`
- `player` : `str` nom du joueur dont la route se casse
- OK: appel de `get_broken_roads(True)`

**Réparer une route sans coût :** `admin_fix_road(player)`
- `player` : `str` nom du joueur dont la route est réparée sans coût
- OK: appel de `get_broken_roads(True)`

**Ajout d'activité sans consommer les actions:** `admin_force_add_activity(player_name, activity_name)`
- `player_name` : `str` nom du joueur
- `activity_name` : `str` nom de l'activité
- OK: même résultat que `try_add_activity`

**Suppression d'activité sans consommer les actions:** `admin_force_remove_activity(player_name, activity_name, nb)`
- `player_name` : `str` nom du joueur
- `activity_name` : `str` nom de l'activité
- `nb` : `int` nombre d'occurences de l'activité à retirer au maximum
- OK: même résultat que `try_remove_activity`

**Modifier une recette d'activité** `admin_change_recipe(player_name, activity_name, modif_dict)`
- `player_name` : `str` nom du joueur
- `activity_name` : `str` nom de l'activité
- `modif_dict` : `dict` modifications apportées aux besoins de la recette (uniquement la différence avec la recette actuelle)
_Note : Le montant de la ressource est modifié dans la partie 'req' ou 'prod' automatiquement_

## Définition des requêtes web (REST)
_Note : Seules les requêtes dynamiques sont spécifiées ici_

**Détails d'une flèche d'import/export d'une ressource :** `/web_client/visu/<player_name>_<res_name>.svg`
- `player_name` : `str` nom du joueur
- `res_name`: `str` nom de la ressource
- OK: envoi de l'image demandée
- KO: erreur `404` et message d'erreur
