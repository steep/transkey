#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import json
import os
import copy


class ActivityRecipe:
    """Contains CONSTANT activity data (recipe), variable data is stored in class Activity
    terrains_req: number of terrains needed
    turn_req: dictionary of resources needs and their quantity for production
    turn_prod: dictionary of resources produced and their quantity
    actions_cost: how many actions taken to place activity
    nb_max: number of times the activity can be placed. "0" indicates infinity
    auto: at the end of the turn, the game tries to set on activity (check resources are available).
    """

    def __init__(self, recipe):
        """Deepcopy recipe if recipe is modified afterward"""
        self.terrains_req = copy.deepcopy(recipe.get("terrains cost", 1))
        self.turn_req = copy.deepcopy(recipe.get("turn requirements")) if recipe.get("turn requirements") else {}
        self.turn_prod = copy.deepcopy(recipe.get("turn products")) if recipe.get("turn products") else {}
        self.actions_cost = copy.deepcopy(recipe.get("actions cost", 1))
        self.nb_max = copy.deepcopy(recipe.get("max number", 0))  # "0" means infinity
        self.auto = copy.deepcopy(recipe.get("auto", False))

    def __repr__(self):
        """Returns the object info"""
        return json.dumps(self.__dict__)


class Activity:
    """Contains activity data (with non-constant info)
    name: activity name
    info: ActivityRecipe used
    running: on or off for the current turn
    new: boolean used for first turn of the activity
    """

    def __init__(self, name, activity_info):
        self.name = name
        self.info: ActivityRecipe = activity_info
        self.running = False
        self.new = True

    def turn_on(self):
        """Action done when placing new activity or player request"""
        self.running = True

    def turn_off(self):
        """Action done when going to next turn"""
        self.running = False
        self.new = False

    def __repr__(self):
        # For JSON serialization, copy object but turn self.info as its repr
        res = copy.deepcopy(self)
        res.info = repr(self.info)
        return json.dumps(res.__dict__)


class ActivityAttributes:
    """Contains activity attributes (image, category, color, auto, base_recipe)"""
    def __init__(self, act_info, config_path, activities_categories):
        """Configure image_path, category and color"""
        self.image_path = ""
        self.category = ""
        self.color = ""

        # If contains image
        if "image" in act_info:
            self.image_path = os.path.join(config_path, act_info["image"])

        # If contains category
        if "category" in act_info:
            self.category = act_info["category"]
            self.color = activities_categories.get(act_info["category"], "")

        self.base_recipe = ""  # Added in other command, only for display

    def add_base_recipe(self, recipe):
        """Configure base_recipe for display on client if they don't have this activity"""
        self.base_recipe = ActivityRecipe(recipe)

    def __repr__(self):
        # For JSON serialization, copy object but turn self.info as its str
        res = copy.deepcopy(self)
        res.base_recipe = str(self.base_recipe)
        return json.dumps(res.__dict__)
