#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import logging
import argparse
import os

from flask import Flask, send_file, redirect
from flask_socketio import SocketIO, emit
from game_state import GameState
from utilities import log_info

# Create Flask application and sockets
app = Flask(__name__,
            template_folder="../web_client",
            static_folder="../",
            static_url_path="")
socketio = SocketIO(app, cors_allowed_origins='*', logger=False)

game = GameState()


# -- Redirect to menu
@app.route('/')
def redirect_menu():
    return redirect('web_client/menu.html')


# -- Socket handling
@socketio.event()
def connect():
    print("New socket created")


@socketio.event()
def disconnect():
    print("Socket destroyed")


@socketio.on("send_msg")
def send_msg(player_name, content):
    """Send a message to everyone via chat"""
    log_info("Message", player_name, content)
    emit("recv_msg", (player_name, content), broadcast=True)


# -- Config API
@socketio.on("load_config")
def load_config(folder_path):
    """Setups new game with requested config"""
    res = game.try_setup(folder_path)
    if not res[0]:
        emit("log", res[1])
        return

    get_players_names()  # Send players info


@socketio.on("get_players_names")
def get_players_names():
    """Get players name as array. Sent in broadcast to force update on menu page"""
    res = game.try_get_players_names()
    if not res[0]:
        emit("log", res[1])
        return
    emit("get_players_names", res[1], broadcast=True)


@socketio.on("rename_player")
def rename_player(old_name, new_name):
    """Rename player before game starting"""
    if not game.setup_done:
        emit("log", "No game setup")
        return
    res = game.try_rename_player(old_name, new_name)
    if not res[0]:
        emit("log", res[1])
        return

    get_players_names()


@socketio.on("force_start")
def force_start():
    """Admin command to force game start"""
    if not game.setup_done:
        emit("log", "No game setup")
        return
    if game.started:
        emit("log", "Game already started")
    game.start()
    emit("game_start", broadcast=True)


# -- Game API
@socketio.on("get_config")
def get_config():
    """Send config info (players list + visible info)"""
    res = game.try_get_config()
    if not res[0]:
        emit("log", res[1])
        return
    emit("get_config", res[1])


@socketio.on("get_info")
def get_info(player_name):
    """Get all info for the client, used when loading or next phase
    Calls every getter useful to load client with every info"""
    if not game.started:
        emit("log", "No game started")
        return

    # Nothing uses broadcast because every client requests its get_info when connecting
    # TODO check how to handle if order of messages is incorrect (get_resources before get_resources_info for example)
    get_resources_info()
    get_activities_info()
    get_turn_info()
    for p in game.players:
        get_resources(p)
        get_terrains(p)
    get_recipes(player_name)
    get_phase_state(player_name)
    get_nb_players_done()
    get_trades(player_name)
    get_nb_actions(player_name)
    get_broken_roads()
    get_fix_road_cost()


def get_turn_info():
    """Get current turn number"""
    emit("get_turn_info", game.get_turn_info())


def get_recipes(player_name):
    """Get player's recipes"""
    res = game.try_get_recipes(player_name)
    if not res[0]:
        emit("log", res[1])
        return
    emit("get_recipes", res[1])


def get_resources_info():
    """Get all resources and attributes"""
    emit("get_resources_info", game.get_resources_info())


def get_activities_info():
    """Get all activities and attributes"""
    emit("get_activities_info", game.get_activities_info())


@socketio.on("get_resources")
def get_resources(player_name, broadcast=False):
    """Get player's resources as dictionary"""
    res = game.try_get_resources(player_name)
    if not res[0]:
        emit("log", res[1])
        return
    emit("get_resources", (player_name, res[1]), broadcast=broadcast)


@socketio.on("get_terrains")
def get_terrains(player_name, broadcast=False):
    """Get player's terrains"""
    res = game.try_get_terrains(player_name)
    if res[0]:
        emit("get_terrains", (player_name, str(res[1][0]), res[1][1]),
             broadcast=broadcast)


def get_nb_actions(player_name):
    """Get player's current actions done and total actions done for current turn"""
    res = game.try_get_nb_actions(player_name)
    if res[0]:
        emit("get_nb_actions", res[1])


@socketio.on("add_activity")
def add_activity(player_name, activity_name):
    """Add new activity for the player requesting it"""
    if not game.started:
        emit("log", "No game started")
        return
    res = game.try_add_activity(player_name, activity_name)
    emit("log", res[1])

    if not res[0]:
        return

    get_terrains(player_name, broadcast=True)
    get_phase_state(player_name)
    get_nb_players_done()
    get_nb_actions(player_name)
    for p in game.players:
        get_resources(p, broadcast=True)


@socketio.on("remove_activity")
def remove_activity(player_name, terrain_id):
    """Remove activity for the player requesting it"""
    if not game.started:
        emit("log", "No game started")
        return
    res = game.try_remove_activity(player_name, terrain_id)
    emit("log", res[1])

    if not res[0]:
        return

    get_terrains(player_name, broadcast=True)
    get_phase_state(player_name)
    get_nb_players_done()
    get_nb_actions(player_name)
    get_resources(player_name, broadcast=True)


@socketio.on("set_activity_state")
def set_activity_state(player_name, terrain_id, state):
    """Update state (on/off) of a given activity for a given player"""
    if not game.started:
        emit("log", "No game started")
        return

    res = game.try_set_activity_state(player_name, terrain_id, state)
    emit("log", res[1])  # Log in chat anyway

    if not res[0]:
        return

    # Update values
    get_terrains(player_name, broadcast=True)
    get_phase_state(player_name)
    get_trade_visu()
    get_nb_players_done()
    emit("info_trade", broadcast=True)  # In case a trade is now impossible
    # Everyone has its resources updated to force update shared resources
    for p in game.players:
        get_resources(p, broadcast=True)


@socketio.on("set_phase_state")
def set_phase_state(player_name, state):
    """Update phase state, if player is ready or not to play next phase"""

    if not game.started:
        emit("log", "No game started")
        return

    res = game.try_set_phase_done(player_name, state)
    if not res[0]:
        emit("log", res[1])
        return

    # Send info for next phase if current is done, else only update state
    if game.check_and_apply_next_phase(app.template_folder):
        emit("next_phase", game.get_turn_info()[0], broadcast=True)
    else:
        get_phase_state(player_name)
        get_nb_players_done()


def get_phase_state(player_name):
    """Get phase state for given player"""
    res = game.try_get_phase_state(player_name)
    if not res[0]:
        emit("log", res[1])
        return
    emit("get_phase_state", res[1])


def get_nb_players_done():
    """Get number of players done and total number of players"""
    # Not checking the return status of the function since this function is called only after game has started
    emit("get_nb_players_done", game.try_get_nb_players_done()[1], broadcast=True)


@socketio.on("start_trade")
def start_trade(player1, player2, res1, res2):
    """Start a new trade between two players"""
    if not game.started:
        emit("log", "No game started")
        return

    if not (isinstance(res1, dict) and isinstance(res2, dict)):
        emit("log", "Incorrect input for resources in trade")
        return

    res = game.try_start_trade(player1, player2, res1, res2)
    if not res[0]:
        emit("log", res[1])
        return

    # Broadcasting to every player the info of new trade or trade update
    emit("info_trade", broadcast=True)


@socketio.on("get_trades")
def get_trades(player_name):
    """Get all current trades for given player"""
    res = game.try_get_trades(player_name)
    if not res[0]:
        emit("log", res[1])
        return

    emit("get_trades", res[1])
    get_trade_visu()  # Sent a lot of times, could be optimized?


@socketio.on("set_trade_state")
def set_trade_state(player_name, trade_id, state):
    """Update trade state for given player (OK, PENDING or KO)"""
    if not game.started:
        emit("log", "No game started")
        return

    res = game.try_set_trade_state(trade_id, player_name, state)
    if not res[0]:
        emit("log", res[1])
        return

    # Send to everyone the info of trades updating
    emit("info_trade", broadcast=True)
    # In case trade is done, update resources
    if res[1]["trade_done"]:
        get_resources(res[1]["names"][0], broadcast=True)
        get_resources(res[1]["names"][1], broadcast=True)


@socketio.on("update_trade")
def update_trade(trade_id, current_player, current_player_res, other_player_res):
    """Update trades resources. If successful, trade gets back to pending"""
    if not game.started:
        emit("log", "No game started")
        return

    if not (isinstance(current_player_res, dict) and isinstance(other_player_res, dict)):
        emit("log", "Incorrect input for resources in trade")
        return

    res = game.try_update_trade(trade_id, current_player, current_player_res, other_player_res)
    if not res[0]:
        emit("log", res[1])
        return

    # Send to everyone the info of trades updating
    emit("info_trade", broadcast=True)


def get_broken_roads(broadcast=False):
    """Get array of players having a broken road"""
    if not game.started:
        emit("log", "No game started")
        return

    emit("get_broken_roads", game.get_broken_roads(), broadcast=broadcast)


def get_fix_road_cost():
    """Get cost of repairing a road"""
    if not game.started:
        emit("log", "No game started")
        return

    emit("get_fix_road_cost", game.get_fix_road_cost())


@socketio.on("fix_road")
def fix_road(current_player, isolated_player):
    """Repair the road of an isolated player (can be themselves)"""
    res = game.try_fix_road(current_player, isolated_player)
    if not res[0]:
        emit("log", res[1])
        return
    get_broken_roads(broadcast=True)
    get_resources(current_player, broadcast=True)
    get_trade_visu()
    emit("info_trade", broadcast=True)  # In case a trade is now impossible


@socketio.on("house")
def house(player_name, nb_housed):
    """Player can change how many employees are housed in their territory"""
    res = game.try_house(player_name, nb_housed)
    if not res[0]:
        emit("log", res[1])
        return

    get_resources(player_name, broadcast=True)


@socketio.on("get_desc")
def get_desc(player_name):
    """Show player's territory description"""
    if not game.started:
        emit("log", "No game started")
        return

    res = game.try_get_desc(player_name)
    if not res[0]:
        emit("log", res[1])
        return

    emit("get_desc", res[1])


@socketio.on("get_objectives")
def get_objectives(player_name):
    """Show player's objectives"""
    if not game.started:
        emit("log", "No game started")
        return

    res = game.try_get_objectives(player_name)
    if not res[0]:
        emit("log", res[1])
        return

    emit("get_objectives", (res[1][0], res[1][1]))


def get_trade_visu():
    res = game.try_get_trade_visu()
    if not res[0]:
        emit("log", res[1])
        return

    emit("trade_visu", res[1], broadcast=True)


@socketio.on("convert_ration")
def convert_ration(player_name, res, qt):
    """Player requests to convert a resource to rations"""
    res = game.try_convert_ration(player_name, res, qt)
    emit("log", res[1])

    get_resources(player_name, broadcast=True)


# -- Admin API
@socketio.on("admin_give_res")
def admin_give_res(player_name, res_dict):
    """Generate resources for player"""
    res = game.try_admin_give_res(player_name, res_dict)
    if not res[0]:
        emit("log", res[1])
        return

    get_resources(player_name, broadcast=True)
    get_trade_visu()
    emit("log", res[1])


@socketio.on("admin_force_next_turn")
def admin_force_next_turn(skip_visu=False):
    if skip_visu:
        game.force_next_turn()
    else:
        game.force_next_turn(export_folder=app.template_folder)
    emit("log", "Admin forced next turn", broadcast=True)
    emit("next_phase", game.get_turn_info()[0], broadcast=True)


@socketio.on("admin_force_next_phase")
def admin_force_next_phase():
    game.force_next_phase(export_folder=app.template_folder)
    emit("log", "Admin forced next phase", broadcast=True)
    emit("next_phase", game.get_turn_info()[0], broadcast=True)


@socketio.on("admin_break_road")
def admin_break_road(player):
    game.trades.break_road(player)
    get_broken_roads(broadcast=True)
    emit("log", f"Road of {player} broken")


@socketio.on("admin_fix_road")
def admin_fix_road(player):
    if player in game.trades.broken_roads:
        game.trades.broken_roads.remove(player)
    get_broken_roads(broadcast=True)
    emit("log", f"Road of {player} repaired")


@socketio.on("admin_force_add_activity")
def admin_force_add_activity(player_name, activity_name):
    """Admin command to force adding an activity while in game
    NOTE : Doesn't work in every situation (assumes activity costs 1 action)"""
    # Check player in game
    if player_name not in game.players:
        return

    game.players[player_name].force_add_activity(activity_name)

    # Update visuals (but not resources since it only affects terrains)
    get_terrains(player_name, broadcast=True)

    emit("log", f"Activity {activity_name} added for {player_name}")


@socketio.on("admin_force_remove_activity")
def admin_force_remove_activity(player_name, activity_name):
    """Force removing first occurrence of activity"""
    # Check player in game
    if player_name not in game.players:
        return

    res = game.force_remove_activity(player_name, activity_name, 1)

    if res > 0:
        get_terrains(player_name, broadcast=True)
        get_phase_state(player_name)
        get_nb_players_done()
        get_nb_actions(player_name)
        get_resources(player_name, broadcast=True)
        emit("log", f"Activity {activity_name} removed once for {player_name}")
    else:
        emit("log", f"{player_name} doesn't have activity {activity_name}")


@socketio.on("admin_change_recipe")
def admin_change_recipe(player_name, activity_name, modif_dict):
    """For given player and activity, change req and prod of activity"""
    res = game.try_admin_change_recipe(player_name, activity_name, modif_dict)
    emit("log", res[1])


# -- Static generation for renders
@app.route('/web_client/visu/<player_name>_<res_name>.svg', methods=['GET'])
def render_trade_visu_res(player_name, res_name):
    """Renders a svg file with detail of flows related to a given resource for a player"""
    # Render file requested
    status, txt = game.try_render_trade_visu_res(player_name, res_name, app.template_folder)

    if status:
        return send_file(os.path.join(app.template_folder, txt), mimetype='image/svg+xml')
    else:
        return txt, 404


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--debug", action='store_true', help="Debug mode (already loads and starts a game)")
    parser.add_argument("-l", "--local", action='store_true', help="Local mode (goes on localhost)")

    args = parser.parse_args()

    if args.debug:
        print(game.try_setup('../config_game_5'))
        game.start()
        game.force_next_phase()
        print(game.try_add_activity("player 1", "Forestry Company"))
        game.try_start_trade("player 1", "player 2", {"wood": 1}, {})
        # game.trades.break_road('player 1')
        # game.trades.break_road('player 2')
    logging.getLogger("geventwebsocket").setLevel("ERROR")

    host = "localhost" if args.local else "0.0.0.0"
    print(f"Running on {host}")
    if not args.local:
        print(f"To find which IP address to use, run 'ifconfig' in another window and select IP address starting with 192.168.")
    socketio.run(app, allow_unsafe_werkzeug=True, host=host, port=5000)  # Loops, has to be last command
