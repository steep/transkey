#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""Used to load game configurations"""

import random
import os
import yaml
from yaml.loader import SafeLoader
from utilities import log_info

from trade import CurrentTrades
from resource import ResourceInfo
from activity import ActivityAttributes
from player import Player

# Turn phases
IDLE, HAZARDS, WATER, MAIN = 0, 1, 2, 3

# YAML files path in folder provided by user
YAML_ACT_FILE = 'config_activities.yaml'
YAML_RES_FILE = 'config_resources.yaml'
YAML_PLAYERS_FILE = 'config_players.yaml'
YAML_GAME_FILE = 'config_game.yaml'
YAML_OBJECTIVES_FILE = 'config_objectives.yaml'
YAML_INIT_STATE_FILE = 'config_initial_state.yaml'


class GameStateSetup:
    """Contains every information about the current game played
        players: dictionary of every Player object (key is player name)
        setup_done: boolean indicating if the game is ready to be played
        started: boolean indicating if the game has started
        resources_dict: dictionary of all resources in the game (key is resource name, values are ResourceInfo)
        shared_resources_values: resources dictionary shared betweens users (checked in priority before personal resources)
        resources_categories: list of all resources categories
        activities_dict: dictionary of all activities in the game and their attributes (ActivityAttributes)
        trades: CurrentTrades() object containing all current trades between players
        turn: current turn played
        nb_turns: number of turns played in total
        phase: current turn phase (HAZARDS, WATER or MAIN)
        visible_info: set of info the player is allowed to see. Not used in the model but info is given for the web display
        shared_objective: str with the description of shared objective
    """

    def __init__(self):
        self.players = {}
        self.setup_done = False
        self.started = False
        self.resources_dict = {}
        self.shared_resources_values = {}
        self.resources_categories = []
        self.activities_dict = {}
        self.trades = CurrentTrades()
        self.turn = 1
        self.nb_turns = 1
        self.phase = WATER
        self.visible_info = set()
        self.shared_objective = ""

    def setup_values(self, game_file_dict):
        """Setup game variables"""

        self.__init__()

        # Check file is not empty
        if not game_file_dict:
            return {}

        if "visible info" in game_file_dict:
            for key in game_file_dict["visible info"]:
                if game_file_dict["visible info"].get(key):
                    self.visible_info.add(key)

        if "fix road cost" in game_file_dict:
            self.trades.set_fix_road_cost(game_file_dict["fix road cost"])

        # One turn if nothing mentioned in the game file
        if "turns" in game_file_dict:
            self.nb_turns = game_file_dict["turns"]

        # Add resources categories
        if "resources categories" in game_file_dict:
            self.resources_categories = game_file_dict["resources categories"]

        # Return categories if they exist
        if "activities categories" in game_file_dict:
            return game_file_dict["activities categories"]
        return {}

    def setup_resources(self, res_file_dict, config_path):
        """Setup game resources"""

        # Check file is not empty
        if not res_file_dict:
            return

        # Create every resource with its attributes
        for res in res_file_dict:
            image_path = ""
            res_info = res_file_dict[res] if res_file_dict[res] else {}  # Empty dict if resource has no info
            if "shared" in res_info:
                self.shared_resources_values[res] = res_info.get("shared")
            if "image" in res_info:
                image_path = os.path.join(config_path, res_info["image"])
            self.resources_dict[res] = ResourceInfo(res_info, image_path)

    def setup_players(self, players_file_dict):
        """Setup players with their initial state"""

        # Check file is not empty
        if not players_file_dict:
            return False

        for player in players_file_dict:
            info = players_file_dict[player] if players_file_dict[player] else {}
            self.players[player] = Player(player, info, self.shared_resources_values, self.resources_dict)

        return True

    def setup_activities(self, act_file_dict, config_path, activities_categories):
        """Setup all activities for all players"""

        # Check file is not empty
        if not act_file_dict:
            return

        for activity in act_file_dict:
            # Add activity to dictionary
            self.activities_dict[activity] = ActivityAttributes(act_file_dict[activity],
                                                                config_path,
                                                                activities_categories)

            # Add recipe for each player
            if "recipes" in act_file_dict[activity]:
                recipe_list = act_file_dict[activity]["recipes"]  # Get recipe list for given activity
                if recipe_list:
                    # Add base_recipe if it exists
                    if "all" in recipe_list:
                        self.activities_dict[activity].add_base_recipe(recipe_list["all"])
                    # For each player, we check which recipe should be added
                    for player in self.players:
                        # Check if player has a special config for this activity
                        if player in recipe_list:
                            if not recipe_list[player].get("allowed", True):
                                # Player doesn't have activity
                                continue
                            else:
                                # Player has special recipe
                                self.players[player].add_recipe(activity, recipe_list[player])
                        # If nothing special, and common recipe if it exists
                        elif "all" in recipe_list:
                            self.players[player].add_recipe(activity, recipe_list["all"])

    def try_setup_objectives(self, objectives_dict) -> (bool, str):
        """Load and distributes objectives for all players
        Note : It is considered that at least one configuration is valid (else, an infinite loop will happen)"""

        if not objectives_dict:
            return True, "Nothing to do"

        # Shared objective
        self.shared_objective = objectives_dict["shared"]

        # Dictionary of all individual objectives
        individual_objectives = objectives_dict["individual"]

        if len(individual_objectives) < len(self.players):
            return False, "Not enough objectives to distribute between players"

        while True:  # While we haven't found a solution yet, keep trying
            # Choose p objectives with p being the number of players
            picked_objectives = random.sample(list(individual_objectives), len(self.players))

            # Check that each objective picked is compatible with player
            done = True
            i = 0
            for p in self.players:
                # Retry if player can't have this objective
                if p not in individual_objectives[picked_objectives[i]]["compatibility"]:
                    done = False
                i += 1
            if done:
                break

        # Distribute objectives between players
        i = 0
        for p in self.players:
            self.players[p].set_objective(individual_objectives[picked_objectives[i]]["description"])
            i += 1

        return True, "Done"

    def try_setup_initial_state(self, init_state_dict) -> (bool, str):
        """Load and distributes initial state for all players
        Note : It is considered that at least one configuration is valid (else, an infinite loop will happen)"""

        if not init_state_dict:
            return True, "Nothing to do"

        if len(init_state_dict) < len(self.players):
            return False, "Not enough initial states to distribute between players"

        while True:  # While we haven't found a solution yet, keep trying
            # Choose p initial states with p being the number of players
            picked_init_state = random.sample(list(init_state_dict), len(self.players))

            # Check that each initial state picked is compatible with player
            done = True
            i = 0
            for p in self.players:
                # Retry if player can't have this initial state
                if p not in init_state_dict[picked_init_state[i]]["compatibility"]:
                    done = False
                i += 1
            if done:
                break

        # Distribute initial states between players
        i = 0
        for p in self.players:
            for act in init_state_dict[picked_init_state[i]]["activities"]:
                self.players[p].force_add_activity(act)
            i += 1

        return True, "Done"

    def try_setup(self, config_path) -> (bool, str):
        """Configure new game with given config files
        Returns a tuple with a boolean to know if setup was successful and a message"""

        # Check if the folder exists
        if not os.path.isdir(config_path):
            return False, "Configuration folder not found"

        # Create players with initial state
        try:
            game_file = load_yaml_file(os.path.join(config_path, YAML_GAME_FILE))
            activities_categories = self.setup_values(game_file)
        except FileNotFoundError:
            return False, f"Game file {YAML_GAME_FILE} not found"

        # Create resources
        try:
            res_file = load_yaml_file(os.path.join(config_path, YAML_RES_FILE))
            self.setup_resources(res_file, config_path)
        except FileNotFoundError:
            return False, f"Resources file {YAML_RES_FILE} not found"

        # Create players with initial state
        try:
            players_file = load_yaml_file(os.path.join(config_path, YAML_PLAYERS_FILE))
        except FileNotFoundError:
            return False, f"Players file {YAML_PLAYERS_FILE} not found"
        if not self.setup_players(players_file):
            return False, f"Players file {YAML_PLAYERS_FILE} is missing info (must have one player)"

        # Load activities info for every player
        try:
            activities_file = load_yaml_file(os.path.join(config_path, YAML_ACT_FILE))
            self.setup_activities(activities_file, config_path, activities_categories)
        except FileNotFoundError:
            return False, f"Activities file {YAML_ACT_FILE} not found"

        # Load and distribute objectives
        try:
            objectives_file = load_yaml_file(os.path.join(config_path, YAML_OBJECTIVES_FILE))
            try_give_objectives = self.try_setup_objectives(objectives_file)
            if not try_give_objectives[0]:
                return try_give_objectives
        except FileNotFoundError:
            log_info("Setup", "Server", f"Objectives file {YAML_OBJECTIVES_FILE} not found, skipped")

        # Load and distribute objectives
        try:
            initial_state_file = load_yaml_file(os.path.join(config_path, YAML_INIT_STATE_FILE))
            try_init_state = self.try_setup_initial_state(initial_state_file)
            if not try_init_state[0]:
                return try_init_state
        except FileNotFoundError:
            log_info("Setup", "Server", f"Initial state file {YAML_INIT_STATE_FILE} not found, skipped")

        self.setup_done = True
        log_info("Setup", "Server", f"Game {config_path} successfully setup")
        return True, f"Game {config_path} successfully setup"


def load_yaml_file(filename):
    """Loads a YAML file as a dictionary"""
    with open(filename) as f:
        config_dict = yaml.load(f, Loader=SafeLoader)
    return config_dict
