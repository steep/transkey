#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
from utilities import log_info, print_dict
from game_state_setup import GameStateSetup, IDLE, HAZARDS, WATER, MAIN
from terrains import Terrain
from player import Player


class GameState(GameStateSetup):
    """Contains every information about the current game played
    players: dictionary of every Player object (key is player name)
    setup_done: boolean indicating if the game is ready to be played
    started: boolean indicating if the game has started
    resources_dict: dictionary of all resources in the game (key is resource name, values are ResourceInfo)
    shared_resources_values: resources dictionary shared betweens users (checked in priority before personal resources)
    resources_categories: list of all resources categories
    activities_dict: dictionary of all activities in the game and their attributes (ActivityAttributes)
    trades: CurrentTrades() object containing all current trades between players
    turn: current turn played
    nb_turns: number of turns played in total
    phase: current turn phase (IDLE, HAZARDS, WATER or MAIN)
    visible_info: set of info the player is allowed to see. Not used in the model but info is given for the web display
    shared_objective: str with the description of shared objective

    Note: functions starting with try_ return a tuple with first element being a boolean indicating success
    """

    def __init__(self):
        super().__init__()

    def try_rename_player(self, old_name, new_name) -> (bool, str):
        """Rename an existing player to give more immersion to players"""
        # Check rename allowed
        if self.started:
            return False, f"Game already started"

        # Update some characters to make sure everything works
        new_name = new_name.replace('"', "'").replace("&", ".")

        # UNUSED CODE: not allowed to rename reserved names
        # if old_name == "common":
        #     return False, f"{old_name} can't be renamed"
        # Check old name exists
        if old_name not in self.players:
            return False, f"old name '{old_name}' doesn't exist"
        # Check new name is not already taken
        if new_name in self.players:
            return False, f"new name '{new_name}' already in the game"

        # Player name change OK
        player_object = self.players.pop(old_name)
        player_object.name = new_name
        self.players[new_name] = player_object
        log_info("Setup", "Server", f"Player \"{old_name}\" renamed to \"{new_name}\"")
        return True, new_name

    def start(self):
        """Start game currently loaded"""
        if self.started or not self.setup_done:
            return

        self.started = True

        # Give new turn resources and log
        for p in self.players:
            player: Player = self.players[p]
            player.add_resources_inventory(player.new_turn_resources)
            log_info("Resources", player.name, player.get_resources(self.resources_dict))

    def _player_action_allowed(self, player_name, check_end=False, phase=None) -> (bool, str):
        """Check if player is allowed to perform an action
        check_end flag is used to check if the game is finished
        returns a tuple containing (status: bool, message: str)
        """
        # Check game started
        if not self.started:
            return False, f"Game not started"

        # Check action is adapted to current phase
        if phase and self.phase != phase:
            return False, f"Action can not be performed in this phase"

        # Check game finished
        if check_end and self.turn > self.nb_turns:
            return False, f"Game finished"

        # Check player in game
        if player_name not in self.players:
            return False, f"Player {player_name} not found"
        return True, "Action can be performed"

    def try_add_activity(self, player_name, activity_name) -> (bool, str):
        """Tries to add a new activity requested by a player"""
        allow_status = self._player_action_allowed(player_name, check_end=True, phase=MAIN)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        res = player.try_add_activity(activity_name)
        if res[0]:
            log_info("Resources", player_name, player.get_resources(self.resources_dict))
        return res

    def try_remove_activity(self, player_name, terrain_id, phase=MAIN) -> (bool, str):
        """Tries to remove an activity"""
        allow_status = self._player_action_allowed(player_name, check_end=True, phase=phase)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        res = player.try_remove_activity(terrain_id)
        if res[0]:
            log_info("Resources", player_name, player.get_resources(self.resources_dict))
        return res

    def force_remove_activity(self, player_name, activity_name, nb_occurrences):
        """Tries to remove activity an activity for a given number of occurrences"""
        nb_removed = 0

        for _ in range(nb_occurrences):
            # Find terrain ID which contains activity
            terrain_id = -1

            for i, act in enumerate(self.players[player_name].terrains.activities_iterator()):
                if act.name == activity_name:
                    terrain_id = i
                    break

            if terrain_id == -1:
                return nb_removed

            if terrain_id >= 0:
                # Give player one action, so they are allowed to remove activity for free
                self.players[player_name].actions_done -= 1
                self.players[player_name].terrains.get_act(terrain_id).new = False
                self.try_remove_activity(player_name, terrain_id, phase=None)
                nb_removed += 1

        return nb_removed

    def try_set_activity_state(self, player_name, terrain_id, state) -> (bool, str):
        """Tries to update the state (on/off) of a given activity from a given player"""
        allow_status = self._player_action_allowed(player_name, check_end=True, phase=MAIN)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        res = player.try_set_activity_state(terrain_id, state, self.resources_dict)
        if res[0]:
            self.trades.check_impossible_trades()  # Modification of resources, check impossible trades
            log_info("Resources", player.name, player.get_resources(self.resources_dict))
        return res

    def try_set_phase_done(self, player_name, state) -> (bool, str):
        """Tries to update player's phase state to indicate if they are ready for next phase."""
        allow_status = self._player_action_allowed(player_name, check_end=True)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        return player.try_set_phase_done(state)

    def try_get_phase_state(self, player_name) -> (bool, str):
        """Tries to get player's current phase state"""
        allow_status = self._player_action_allowed(player_name)
        if not allow_status[0]:
            return allow_status
        return True, self.players[player_name].get_phase_done()

    def get_resources_info(self):
        """Get all resources and attributes"""
        return self.resources_categories, str(self.resources_dict).replace("'", '"')

    def get_activities_info(self):
        """Get all activities and attributes"""
        return print_dict(self.activities_dict)

    def get_turn_info(self):
        """Get info about the current turn"""
        return self.phase, self.turn, self.nb_turns

    def try_start_trade(self, player1, player2, res1, res2) -> (bool, str):
        """Tries to initiate a trade between two players"""
        if player1 == player2:
            return False, f"Trade has to be done between different players"
        # Check players are in game
        for p in [player1, player2]:
            allow_status = self._player_action_allowed(p, check_end=True, phase=MAIN)
            if not allow_status[0]:
                return allow_status

        return self.trades.try_start_trade(self.players[player1], self.players[player2], res1, res2, self.resources_dict)

    def try_update_trade(self, trade_id, current_player, own_res, other_res) -> (bool, str | list):
        """Tries to update the trade offer (which resources are offered and requested)"""
        allow_status = self._player_action_allowed(current_player, check_end=True, phase=MAIN)
        if not allow_status[0]:
            return allow_status

        return self.trades.try_update_trade(trade_id, current_player, own_res, other_res, self.resources_dict)

    def try_set_trade_state(self, trade_id, current_player, state) -> (bool, str | dict):
        """Tries to update the trade state (OK, PENDING or KO)"""
        allow_status = self._player_action_allowed(current_player, check_end=True, phase=MAIN)
        if not allow_status[0]:
            return allow_status

        res = self.trades.try_set_trade_state(trade_id, current_player, state, self.resources_dict)
        # Modification of resources, check impossible trades and housing
        if res[0] and res[1]["trade_done"]:
            self.trades.check_impossible_trades()
            for player in res[1]["names"]:
                self.players[player].check_impossible_housing(self.resources_dict)

        return res

    def try_fix_road(self, current_player_name, isolated_player_name) -> (bool, str):
        """Tries to repair the road of an isolated player (can be themselves)"""
        # Check players are in game
        for p in [current_player_name, isolated_player_name]:
            allow_status = self._player_action_allowed(p, check_end=True, phase=MAIN)
            if not allow_status[0]:
                return allow_status
        player: Player = self.players[current_player_name]

        res = self.trades.try_fix_road(player, isolated_player_name)
        if res[0]:
            self.trades.check_impossible_trades()  # Modification of resources, check impossible trades
        return res

    def get_broken_roads(self):
        """Player gets the list of broken roads (players not allowed to trade)"""
        return self.trades.get_broken_roads()

    def get_fix_road_cost(self):
        """Player gets fix road cost"""
        return self.trades.get_fix_road_cost()

    def try_get_players_names(self) -> (bool, list[str]):
        """Tries to get list of players/territories names"""
        if not (self.setup_done and self.players):
            return False, f"No game setup"

        return True, [p for p in self.players]

    def try_get_config(self) -> (bool, str | tuple[list, list]):
        """Tries to get current game config"""
        if not (self.setup_done and self.players):
            return False, f"No game setup"

        return True, ([p for p in self.players], list(self.visible_info))

    def try_get_desc(self, player_name) -> (bool, str):
        """Tries to give player's territory description"""
        # Check player in game
        if player_name not in self.players:
            return False, f"Player {player_name} not found"

        return True, self.players[player_name].get_desc()

    def try_get_objectives(self, player_name) -> (bool, str | list[str]):
        """Tries to give player's objectives"""
        # Check player in game
        if player_name not in self.players:
            return False, f"Player {player_name} not found"

        return True, [self.shared_objective, self.players[player_name].get_objective()]

    def try_get_nb_actions(self, player_name) -> (bool, str | list[int]):
        """Get player's current actions done and total actions done for current turn"""
        allow_status = self._player_action_allowed(player_name)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        return True, player.get_nb_actions()

    def try_get_resources(self, player_name) -> (bool, str | dict):
        """Tries to get player's resources as dictionary"""
        allow_status = self._player_action_allowed(player_name)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        return True, player.get_resources(self.resources_dict)

    def try_get_recipes(self, player_name) -> (bool, str):
        """Tries to get player's recipes for activities"""
        allow_status = self._player_action_allowed(player_name)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        return True, player.get_recipes()

    def try_get_terrains(self, player_name) -> (bool, str | tuple[list[Terrain], tuple[int, int]]):
        """Tries to get player's terrains"""
        allow_status = self._player_action_allowed(player_name)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        return True, player.get_terrains()

    def try_get_trades(self, player_name) -> (bool, str | list):
        """Tries to get player's current trades"""
        allow_status = self._player_action_allowed(player_name)
        if not allow_status[0]:
            return allow_status

        return True, self.trades.get_trades(player_name)

    def try_get_trade_visu(self) -> (bool, str):
        """Tries to get current state of imports/exports of every player"""
        # Check game started
        if not self.started:
            return False, f"Game not started"

        return True, print_dict(self.players, lambda d, e: d[e].data_trade_visu)  # Only print data_trade_visu

    def try_house(self, player_name, nb_housed) -> (bool, str):
        """Tries to house employees"""
        allow_status = self._player_action_allowed(player_name, phase=MAIN)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        return player.try_house(nb_housed, self.resources_dict)

    def try_convert_ration(self, player_name, res, nb) -> (bool, str):
        """Tries to convert resources into rations"""
        allow_status = self._player_action_allowed(player_name, phase=MAIN)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        return player.try_convert_ration(res, nb, self.resources_dict)

    def try_get_nb_players_done(self) -> (bool, str | tuple[int, int]):
        """Tries to get number of players done and total number of players for the current turn"""
        # Check game started
        if not self.started:
            return False, f"Game not started"
        nb_done = self.nb_players_done()
        return True, (nb_done, len(self.players))

    def try_admin_give_res(self, player_name, res_dict) -> (bool, str):
        """Force player to get resources"""
        allow_status = self._player_action_allowed(player_name)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        player.give_res(res_dict)
        return True, f"{player_name} received {res_dict}"

    def try_admin_change_recipe(self, player_name, activity_name, modif_dict) -> (bool, str):
        """For given player and activity, change req and prod of activity"""
        allow_status = self._player_action_allowed(player_name)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        return player.try_admin_change_recipe(activity_name, modif_dict)

    def nb_players_done(self):
        """Update number of players done"""
        # Get number of players done with the current phase
        nb_players_done = 0
        for p in self.players:
            if self.players[p].phase_done:
                nb_players_done += 1
        return nb_players_done

    def check_and_apply_next_phase(self, export_folder=None):
        """Call this function to check and apply if next phase should start
        Note: it is called by the server when a player's phase state changes
        (Not automatically done in model to keep messages order)
        Returns True if phase has changed"""

        # Check game already finished
        if self.turn > self.nb_turns:
            return False, self.phase

        # Check everyone is done
        if self.nb_players_done() < len(self.players):
            return False

        # -- Everything OK : Go next phase

        # Reset player phase state
        for p in self.players:
            self.players[p].try_set_phase_done(False)

        # End current phase and set variables for next
        if self.phase == IDLE:
            self.phase = HAZARDS
            for p in self.players:
                self.players[p].reset_visu_trades()
            return True
        if self.phase == HAZARDS:
            self.phase = WATER
            return True
        if self.phase == WATER:
            self.phase = MAIN
            return True
        if self.phase == MAIN:
            # Reset shared resources once if necessary
            for res in self.resources_dict:
                if self.resources_dict[res].shared and self.resources_dict[res].reset:
                    self.shared_resources_values[res] = 0

            # Create folder for Sankey visualizations renders
            if export_folder and not os.path.exists(os.path.join(export_folder, "render")):
                os.makedirs(os.path.join(export_folder, "render"))

            # Set player variables for next turn and export visualizations
            for index in self.players:
                player: Player = self.players[index]

                filename = None
                if export_folder:
                    filename = os.path.join(export_folder, "render", f"{player.name}_{self.turn}.svg")

                player.next_turn(self.resources_dict, self.activities_dict, filename)

            self.phase = IDLE
            self.turn += 1
            log_info("Global", "Server", f"New turn (turn {self.turn})")
            return True

    def force_next_turn(self, export_folder=None):
        """Force next turn even if people have not validated their turn"""
        self.force_next_phase(export_folder)
        # Skip phases until next turn
        while self.phase != IDLE:
            self.force_next_phase(export_folder)

    def force_next_phase(self, export_folder=None):
        """Force next phase even if people have not validated their turn"""

        # Check game finished
        if self.turn > self.nb_turns:
            return

        for p in self.players:
            self.players[p].phase_done = True
        self.check_and_apply_next_phase(export_folder)

    def try_render_trade_visu_res(self, player_name, res_name, folder) -> (bool, str):
        """Render the visualization of a given resource for a player"""

        allow_status = self._player_action_allowed(player_name)
        if not allow_status[0]:
            return allow_status
        player: Player = self.players[player_name]

        if res_name not in self.resources_dict:
            return False, f"Resource '{res_name}' does not exist"

        # Create folder for Sankey visualizations renders
        if folder and not os.path.exists(os.path.join(folder, "render")):
            os.makedirs(os.path.join(folder, "render"))

        filename = os.path.join("render", f"{player_name}_{res_name}.svg")
        return player.data_trade_visu.try_render_trade_sankey(res_name, self.resources_dict, folder, filename)
