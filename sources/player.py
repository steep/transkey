#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from utilities import log_info, print_dict
from activity import Activity, ActivityRecipe
from data_visualization import DataVisu
from trade_data_visualization import TradeVisu
from terrains import Terrains, Terrain


class Player:
    """Contains player data
    name: player name (unique and constant)
    desc: territory text description
    resources: every resource and quantity the player has
    shared_resources: pointer to shared resources for all players
    new_turn_resources: which resources and quantities are given to the player at each turn beginning
    max_actions: number of actions that can be performed each turn (constant)
    actions_done: number of actions currently done for this turn
    phase_done: boolean indicating if player is done for this phase
    activities_recipes: dictionary of possible ActivityRecipe to add for the player
    terrains: contains player's Terrains object
    data_visu: DataVisu object containing info for Sankey rendering at end of turn
    data_trade_visu: TradeVisu object containing info for import/export data visualization
    objective: str with the description of individual objective
    """

    def __init__(self, name, info, shared_resources, resources_dict):
        self.name = name
        self.desc = info.get("description", "")
        self.resources = info.get("resources") if info.get("resources") else {}
        self.shared_resources = shared_resources
        self.new_turn_resources = info.get("new turn resources", {})
        self.max_actions = info.get("max actions", 0)
        self.actions_done = 0
        self.phase_done = False
        self.activities_recipes = dict()
        self.terrains = Terrains(info.get("terrains", 0))
        self.data_visu = DataVisu(self.resources, resources_dict)
        self.data_trade_visu = TradeVisu()
        self.objective = ""

    def set_objective(self, objective):
        self.objective = objective

    def get_objective(self):
        return self.objective

    def add_resources_inventory(self, res_dict):
        """Add values of a dictionary into another
        NOTE : Every resource is constrained to be 0 at minimum"""
        if not res_dict:
            return
        for res in res_dict:
            # shared resource
            if res in self.shared_resources:
                old_value = self.shared_resources[res]
                self.shared_resources[res] = max(0, self.shared_resources[res] + res_dict[res])
                self.data_trade_visu.add_shared(res, self.shared_resources[res] - old_value)

            # individual resource
            else:
                if res not in self.resources:
                    self.resources[res] = 0
                self.resources[res] = max(0, self.resources[res] + res_dict[res])

    def subtract_resources_inventory(self, res_dict):
        """Subtract values from a dictionary into another"""
        if not res_dict:
            return
        for res in res_dict:
            # shared resource
            if res in self.shared_resources:
                self.shared_resources[res] -= res_dict[res]
                self.data_trade_visu.add_shared(res, -res_dict[res])
            # individual resource
            else:
                if res not in self.resources:
                    self.resources[res] = 0
                self.resources[res] -= res_dict[res]

    def reset_resources_inventory(self, res_dict):
        """Reset every resource with reset flag in res_dict"""
        if not res_dict:
            return
        for res in res_dict:
            if not res_dict[res].shared and res_dict[res].reset:
                self.resources[res] = 0

    def give_res(self, res_dict):
        """Force player to get resources"""
        self.add_resources_inventory(res_dict)
        self.data_visu.add_give_res(self.resources, res_dict)

    def check_has_enough_resources(self, res_dict):
        """Check the player can consume given number of resources"""
        if res_dict:
            for res in res_dict:
                if res in self.shared_resources:
                    if self.shared_resources[res] < res_dict[res]:
                        return False
                elif res in self.resources:
                    if self.resources[res] < res_dict[res]:
                        return False
                elif res_dict[res] > 0:
                    return False
        return True

    def add_recipe(self, activity_name, recipe):
        """Add new activity recipe"""
        self.activities_recipes[activity_name] = ActivityRecipe(recipe)

    def try_admin_change_recipe(self, activity_name, modif_dict) -> (bool, str):
        """For given player and activity, change req and prod of activity"""
        if activity_name not in self.activities_recipes:
            return False, f"Activity not found for {self.name}"

        altered_recipe = self.activities_recipes[activity_name]

        # Change req
        if modif_dict and isinstance(modif_dict, dict):
            for res in modif_dict:
                if res in altered_recipe.turn_req:
                    altered_recipe.turn_req[res] += modif_dict[res]
                elif res in altered_recipe.turn_prod:
                    altered_recipe.turn_prod[res] += modif_dict[res]

        self.activities_recipes[activity_name] = altered_recipe
        return True, f"Recipe of {activity_name} for {self.name} updated"

    def get_nb_actions(self):
        """Get player current number of actions done and total allowed"""
        return [self.actions_done, self.max_actions]

    def get_resources(self, resources_dict):
        """Returns player's resources by also putting the shared resources in the returned dictionary"""

        merge_dict = dict()
        # Resources are kept in order and put to 0 in case player doesn't have any
        for res in resources_dict:
            merge_dict[res] = 0

        # Update values of each resource
        merge_dict.update(self.resources)
        merge_dict.update(self.shared_resources)

        return merge_dict

    def get_recipes(self):
        """Return json string containing all activity recipes"""
        return print_dict(self.activities_recipes)

    def get_terrains(self) -> (list[Terrain], (int, int)):
        """Return player's terrains"""
        return self.terrains.get_terrains()

    def get_desc(self):
        """Returns player's territory description"""
        return self.desc

    def try_add_activity(self, activity_name) -> (bool, str):
        """Tries to add a new activity requested by a player"""

        # Check recipe exists for given player
        if activity_name not in self.activities_recipes:
            return False, f"Player {self.name} can't build {activity_name}"
        activity = Activity(activity_name, self.activities_recipes[activity_name])

        # Check player still has enough actions
        if self.actions_done + activity.info.actions_cost > self.max_actions:
            return False, f"Not enough territory modifications remaining"

        # Count number of times activity is already in terrains
        nb_same_activity_placed = self.terrains.get_nb_occurrences(activity_name)
        if activity.info.nb_max != 0 and nb_same_activity_placed >= activity.info.nb_max:
            return False, f"Only allowed to place {activity_name} {activity.info.nb_max} times"

        # REMOVED MECHANIC: activity directly "on"
        # # Check player has enough resources
        # if not self.check_has_enough_resources(activity.info.turn_req):
        #     return False, f"Missing resource"

        # -- Everything here OK - Try to add on terrains

        status, txt = self.terrains.try_add_activity(activity)

        # Failed to add activity, abort
        if not status:
            return False, txt

        # REMOVED MECHANIC: activity directly "on"
        # # Activity is instantly set on
        # self.try_set_activity_state(len(self.terrains)-1, 'on')

        self.actions_done += activity.info.actions_cost

        # Player's phase is not considered done anymore if they change something
        self.phase_done = False

        log_info("Activity", self.name, f"\"{activity_name}\" added")
        return True, f"Activity {activity_name} added"

    def force_add_activity(self, activity_name):
        """Force add a new activity for a player without any cost in actions
        Checks player can add activity and has enough terrains,
        but this function should only be called when it's possible"""
        # Check recipe exists for given player
        if activity_name not in self.activities_recipes:
            return
        activity = Activity(activity_name, self.activities_recipes[activity_name])
        activity.turn_off()

        # Try to force add activity and remove terrains
        status, txt = self.terrains.try_add_activity(activity)

        # Failed to add activity, abort
        if not status:
            return False, txt

    def try_remove_activity(self, terrain_id) -> (bool, str):
        """Tries to remove the activity from user's terrains.
        Costs one action and can not be performed on new activities"""

        # Check terrain exists
        if not isinstance(terrain_id, int):
            return False, f"terrain_id must be an int"

        activity = self.terrains.get_act(terrain_id)
        if not activity:
            return False, f"No activity found on this terrain"

        if activity.new:
            return False, f"Can't remove new activity '{activity.name}'"

        if activity.running:
            return False, f"Can't remove running activity '{activity.name}'"

        if self.actions_done + 1 > self.max_actions:
            return False, f"Not enough territory modifications remaining to remove '{activity.name}'"

        # Give back terrains but costs 1 action
        self.terrains.remove_activity(terrain_id)
        self.actions_done += 1

        # Player's phase is not done anymore since they changed something
        self.phase_done = False

        log_info("Activity", self.name, f"\"{activity.name}\" removed (1 action)")
        return True, f"Activity '{activity.name}' was removed (1 action)"

    def try_set_activity_state(self, terrain_id: int, state, resources_dict) -> (bool, str):
        """Tries to update the state of a given activity from the player
        "on" activates the activity
        "off" originally had the effect of a ctrl-z but was not kept, still here in case the mechanic is back
        """

        # Check terrain exists
        if not isinstance(terrain_id, int):
            return False, f"terrain_id must be an int"

        activity = self.terrains.get_act(terrain_id)
        if not activity:
            return False, f"No activity found on this terrain"

        # Check state is "on" or "off"
        if state not in ["on", "off"]:
            return False, f"state has to be either on or off."

        if state == "on":
            # Put activity on (turn requirements are consumed)
            if not activity.running:
                if not self.check_has_enough_resources(activity.info.turn_req):
                    return False, f"Missing resources to put '{activity.name}' on"
                activity.turn_on()
                self.subtract_resources_inventory(activity.info.turn_req)
                self.add_resources_inventory(activity.info.turn_prod)
                self.data_visu.add_act(self.resources, activity)

            if has_housing(resources_dict):
                # Auto-housing mechanic : If new activity can house n employees, then add max housing possible (up to n)
                if activity.info.turn_prod.get("housing"):
                    nb_housed = self.resources.get("housed employee", 0)
                    new_housed = activity.info.turn_prod.get("housing")
                    max_housed = self.resources.get("employee", 0)
                    self.try_house(min(nb_housed + new_housed, max_housed), resources_dict)

                # Auto-housing mechanic 2 : New employees are housed if possible
                if activity.info.turn_prod.get("employee"):
                    nb_housed = self.resources.get("housed employee", 0)
                    nb_new = activity.info.turn_prod.get("employee", 0)
                    max_housed = self.resources.get("housing", 0)
                    self.try_house(min(nb_housed + nb_new, max_housed), resources_dict)

            # Player's phase is not done anymore since they changed something
            self.phase_done = False

            log_info("Activity", self.name, f"\"{activity.name}\" set on")
            return True, f"Activity '{activity.name}' set on"
        else:
            log_info("Activity", "Server", "Activity manually set off was removed")
            return False, f"Activity manually set off was removed"

        # TURN STATE OFF - MECHANIC NOT KEPT
        # else:
        #     activity = self.terrains[terrain_id]
        #     if activity.running:
        #         activity.running = False
        #         self.add_resources_inventory(activity.info.turn_req)
        #     # Player's phase is not done anymore since they changed something
        #     self.phase_done = False

    def try_set_phase_done(self, state) -> (bool, str):
        """Tries to update player's phase state to indicate if they are ready for next phase"""
        # Check state validity
        if not isinstance(state, bool):
            return False, f"state has to be either True or False."

        self.phase_done = state

        return True, f"Player phase state updated to {state}"

    def get_phase_done(self) -> bool:
        return self.phase_done

    def try_house(self, nb_housed, resources_dict) -> (bool, str):
        """Tries to house employees"""
        if not has_housing(resources_dict):
            return False, f"Resources not existing for housing mechanic"

        if nb_housed < 0:
            return False, f"Can't house a negative number of employees"

        if nb_housed > self.resources.get("housing", 0) or nb_housed > self.resources.get("employee", 0):
            return False, f"Missing capacity to house {nb_housed} employees"

        self.resources["housed employee"] = nb_housed
        return True, f"{nb_housed} employees housed"

    def check_impossible_housing(self, resources_dict):
        """Constrain housing to maximum number allowed"""
        if not has_housing(resources_dict):
            return

        self.resources["housed employee"] = min(self.resources.get("housed employee", 0),
                                                self.resources.get("employee", 0))

    def auto_house_trade(self, resources_dict, nb_new):
        """Auto-housing mechanic 3: Employees from trade are automatically housed if possible"""
        if has_housing(resources_dict) and nb_new:
            nb_housed = self.resources.get("housed employee", 0)
            max_housed = self.resources.get("housing", 0)
            self.try_house(min(nb_housed + nb_new, max_housed), resources_dict)

    def try_convert_ration(self, res, nb, resources_dict) -> (bool, str):
        """Convert a given resource to rations. Only works if resource has a ration value"""
        if res not in resources_dict:
            return False, f"Resource {res} does not exist"

        if nb <= 0:
            return False, f"Number to convert must be positive"

        if resources_dict[res].ration_value <= 0:
            return False, f"Resource {res} can't be converted into rations"

        if self.resources.get(res, 0) < nb:
            return False, f"Missing some {res}"

        self.subtract_resources_inventory({res: nb})
        self.add_resources_inventory({"ration": nb * resources_dict[res].ration_value})
        self.data_visu.add_rations(self.resources, {res: nb}, {"ration": nb * resources_dict[res].ration_value})
        return True, f"Successfully converted {nb} {res} to rations"

    def next_turn(self, resources_dict, activities_dict, filename=None):
        """Set variables for next turn and export visualizations"""

        # Every employee not housed adds 1 to the GHG
        if has_housing(resources_dict):
            employees_outside = self.resources.get("employee", 0) - self.resources.get("housed employee", 0)
            if employees_outside > 0:
                self.add_resources_inventory({"greenhouse gas": employees_outside})

        # Auto turn on protected park
        for act_id, act in enumerate(self.terrains.activities_iterator()):
            if act.info.auto:
                self.try_set_activity_state(act_id, "on", resources_dict)

        if filename:
            img = self.data_visu.render_sankey(resources_dict, activities_dict)
            img.savefig(filename)
            img.close()

        # Update player actions turn state
        self.phase_done = False
        self.actions_done = 0

        # Reset resources containing reset flag
        self.reset_resources_inventory(resources_dict)

        # Turn off every activity
        for activity in self.terrains.activities_iterator():
            # REMOVED BEHAVIOUR : PLAYER RECEIVES RESOURCES AT END OF TURN
            # # Activity products for first turn end
            # if activity.is_first_turn:
            #     self.add_resources_inventory(activity.info.placement_prod_turn_end)
            # activity.is_first_turn = False

            # REMOVED BEHAVIOUR: RECEIVING RESOURCES AT END OF TURN
            # # If activity was on
            # if activity.running:
            #     # Add each product to player resources
            #     self.add_resources_inventory(activity.info.turn_prod)

            # Set activity off for next turn, player can put it back on
            activity.turn_off()

        self.add_resources_inventory(self.new_turn_resources)
        self.data_visu = DataVisu(self.resources, resources_dict)

    def reset_visu_trades(self):
        self.data_trade_visu = TradeVisu()


def has_housing(resources_dict):
    return ("employee" in resources_dict
            and "greenhouse gas" in resources_dict
            and "housing" in resources_dict
            and "housed employee" in resources_dict)
