#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""Testfile used to check the model (not the network communication !)
Has to be executed from its own folder (/sources)
Every scenario should only call game functions"""

import os
import sys
from game_state import GameState

# FOLDERS USED (relative to this file)
GAME_SETUP_FOLDER = '../tests_configs/game_setup'
ACTIVITY_FOLDER = '../tests_configs/activity'
NEXT_TURN_FOLDER = '../tests_configs/next_turn'
SHARED_RES_FOLDER = '../tests_configs/shared_res'
TRADE_FOLDER = '../tests_configs/trade'
EMPLOYEES_FOLDER = '../tests_configs/employees'
OBJECTIVES_FOLDER = '../tests_configs/objectives'
INIT_STATE_FOLDER = '../tests_configs/init_state'

game = GameState()


def test(function, folder, export=False, debug=False):
    """Launch a test to check game's functionality"""
    print(f"\033[4m--- Launching {function.__name__} ---\033[0m")
    if export:
        print("WARNING! OUTPUT FILES ARE REPLACED / BEING GENERATED, CONTENT SHOULD BE CHECKED")

    c = Checker(folder, function.__name__, export, debug)
    function(c)

    print(f"Test OK")


class Checker:
    def __init__(self, folder, test_name, export, debug):
        self.folder = folder
        self.expected_nb = 0
        self.test_name = test_name
        self.export = export  # Boolean to create expected files
        self.debug = debug    # Boolean for console debug

    def check_equal_file(self, tuple_checked):
        """Check the result string is the one expected, contained in the test folder"""
        string = str(tuple_checked[1])
        file = f"{self.test_name}_{self.expected_nb}.txt"
        self.expected_nb += 1

        if self.export:  # Used to generate the files (use with caution)
            open(os.path.join(self.folder, file), 'w').write(string)   # USED TO GENERATE (CHECK IF EXPECTED BEHAVIOUR)

        if self.debug:
            print(string)

        try:
            file_content = open(os.path.join(self.folder, file)).read()
            if string == file_content:
                return
            print(f"\033[91m--- Test error ({self.folder}, {file}) ---\033[0m")
            print("\033[4mExpected:\033[0m")
            print(file_content)
            print("\033[4mGot:\033[0m")
            print(string)
            if not self.debug:
                sys.exit(1)
        except FileNotFoundError:
            print(f"File {file} not found")
            # sys.exit(1)


def test_game_setup(c):
    """Test game setup (reading configuration files work)"""
    # Game setup
    c.check_equal_file(game.try_setup(c.folder))
    c.check_equal_file(game.try_rename_player('player 1', 'new name'))
    game.start()
    c.check_equal_file(game.try_rename_player('new name', 'too late'))
    # Player recipes info
    c.check_equal_file(game.try_get_recipes('new name'))
    c.check_equal_file(game.try_get_recipes('player 2'))
    c.check_equal_file(game.try_get_recipes('player 3'))
    # Other getters
    c.check_equal_file(game.try_get_resources('new name'))
    c.check_equal_file(game.try_get_nb_actions('player 2'))


def try_get_terrains_without_random(player_name):
    """Call try_get_terrains but doesn't show size and pos which is random because of z3"""
    res = game.try_get_terrains(player_name)
    if not res[0]:
        return res
    else:
        res_without_random = [terrain.act for terrain in res[1][0]]
        return True, res_without_random


def test_activity(c):
    """Test activity placement, modification of resources, get_terrains"""
    # Game setup
    game.try_setup(c.folder)
    game.start()
    game.force_next_phase()
    # Try adding activities (1st: OK, 2nd: missing resource B, 3rd: player not in game, 4th: not in activities list)
    game.try_add_activity('player 1', 'activity A')
    c.check_equal_file(game.try_set_activity_state('player 1', 0, 'on'))
    c.check_equal_file(game.try_get_nb_actions('player 1'))
    game.try_add_activity('player 1', 'activity A')
    c.check_equal_file(game.try_set_activity_state('player 1', 1, 'on'))
    c.check_equal_file(game.try_add_activity('player 2', 'activity A'))
    c.check_equal_file(game.try_add_activity('player 1', 'activity Z'))
    c.check_equal_file(game.try_get_nb_actions('player 1'))
    # Check resources and terrains after build
    c.check_equal_file(game.try_get_resources('player 1'))
    c.check_equal_file(try_get_terrains_without_random('player 1'))
    game.force_next_turn()
    game.force_next_phase()
    game.force_next_phase()
    game.force_next_phase()
    c.check_equal_file(game.try_get_resources('player 1'))
    # Now missing resource to put activity on
    c.check_equal_file(game.try_set_activity_state('player 1', 0, 'on'))
    c.check_equal_file(try_get_terrains_without_random('player 1'))
    # Test limit of actions number
    c.check_equal_file(game.try_add_activity('player 1', 'activity B'))
    c.check_equal_file(game.try_remove_activity('player 1', 2))
    c.check_equal_file(game.try_add_activity('player 1', 'activity B'))
    game.force_next_turn()
    game.force_next_phase()
    game.force_next_phase()
    game.force_next_phase()
    c.check_equal_file(game.try_remove_activity('player 1', 2))
    c.check_equal_file(try_get_terrains_without_random('player 1'))


def test_next_turn(c):
    """Test functionality to end a phase of a turn"""
    game.try_setup(c.folder)
    game.start()
    game.force_next_phase()
    # Server should automatically call check_and_apply_next_turn() everytime
    c.check_equal_file(game.try_set_phase_done('player 1', True))
    game.check_and_apply_next_phase()
    c.check_equal_file(game.try_get_nb_players_done())
    c.check_equal_file(game.try_set_phase_done('player 1', False))
    game.check_and_apply_next_phase()
    c.check_equal_file(game.try_get_nb_players_done())
    c.check_equal_file(game.try_set_phase_done('player 1', True))
    game.check_and_apply_next_phase()
    c.check_equal_file(game.try_set_phase_done('player 2', True))
    game.check_and_apply_next_phase()
    # Next turn applied (both players "ok"), nb_players_done back to 0
    c.check_equal_file(game.try_get_nb_players_done())
    # Game finished
    c.check_equal_file(game.try_set_phase_done('player 1', True))


def test_shared_resource(c):
    """Tests a shared resource is influenced by any player move"""
    c.check_equal_file(game.try_setup(c.folder))
    game.start()
    game.force_next_phase()
    # Player generates shared resources for an activity, the other can see the value evolve as well
    c.check_equal_file(game.try_get_resources('player 1'))
    c.check_equal_file(game.try_get_resources('player 2'))
    c.check_equal_file(game.try_add_activity('player 1', 'activity'))
    game.try_set_activity_state('player 1', 0, 'on')
    c.check_equal_file(game.try_get_resources('player 1'))
    c.check_equal_file(game.try_get_resources('player 2'))
    # Player uses shared resources for an activity
    game.try_add_activity('player 1', 'another one')
    c.check_equal_file(game.try_set_activity_state('player 1', 1, 'on'))
    game.try_add_activity('player 1', 'another one')
    c.check_equal_file(game.try_set_activity_state('player 1', 2, 'on'))


def test_trade(c):
    """Tests two players trading resources"""
    c.check_equal_file(game.try_setup(TRADE_FOLDER))
    game.start()
    game.force_next_phase()
    # Incorrect usages + Player 1 creates trade proposition with player 2
    c.check_equal_file(game.try_start_trade('player 1', 'player 1', {'A': 1}, {'B': 1}))
    c.check_equal_file(game.try_start_trade('player 3', 'player 2', {'A': 1}, {'B': 1}))
    c.check_equal_file(game.try_start_trade('player 1', 'player 2', {'D': 1}, {'D': 1}))
    c.check_equal_file(game.try_start_trade('player 1', 'player 2', {'A': 1}, {'B': 1}))
    c.check_equal_file(game.try_get_trades('player 1'))
    # Incorrect usages + Player 2 makes a modification to the current proposition
    c.check_equal_file(game.try_update_trade(2, 'player 2', {'B': 1}, {'A': 1, 'C': 1}))
    c.check_equal_file(game.try_update_trade(0, 'player 3', {'B': 1}, {'A': 1, 'C': 1}))
    c.check_equal_file(game.try_update_trade(0, 'player 2', {'B': 10}, {'A': 1, 'C': 1}))
    c.check_equal_file(game.try_update_trade(0, 'player 2', {'B': 1}, {'A': 1, 'C': 1}))
    c.check_equal_file(game.try_get_trades('player 1'))
    # Player 2 makes proposition pending
    c.check_equal_file(game.try_set_trade_state(0, 'player 2', 'PENDING'))
    c.check_equal_file(game.try_get_trades('player 1'))  # Check trade not done
    # Player 1 accepts trade
    c.check_equal_file(game.try_set_trade_state(0, 'player 1', 'OK'))
    c.check_equal_file(game.try_get_trades('player 1'))  # Check trade not done
    # Player 2 accepts trade
    c.check_equal_file(game.try_set_trade_state(0, 'player 2', 'OK'))
    # Trade is done
    c.check_equal_file(game.try_get_resources('player 1'))
    c.check_equal_file(game.try_get_resources('player 2'))
    c.check_equal_file(game.try_get_trades('player 1'))
    # Trade doesn't exist anymore
    c.check_equal_file(game.try_set_trade_state(0, 'player 2', 'OK'))
    # Both players get the info of trade done, then it is deleted
    c.check_equal_file(game.try_get_trades('player 2'))
    c.check_equal_file(game.try_get_trades('player 1'))


def test_trade_impossible(c):
    """Tests two players with resources they don't have then aborting trade"""
    c.check_equal_file(game.try_setup(c.folder))
    game.start()
    game.force_next_phase()
    # Start trade
    c.check_equal_file(game.try_start_trade('player 2', 'player 1', {'A': 10}, {'B': 10}))
    c.check_equal_file(game.try_get_trades('player 1'))
    # They try to accept but can't
    c.check_equal_file(game.try_set_trade_state(0, 'player 1', 'OK'))
    c.check_equal_file(game.try_set_trade_state(0, 'player 2', 'OK'))
    # Player refuses trade, it is then canceled
    c.check_equal_file(game.try_set_trade_state(0, 'player 1', 'KO'))
    c.check_equal_file(game.try_get_trades('player 1'))
    c.check_equal_file(game.try_get_trades('player 2'))
    c.check_equal_file(game.try_get_trades('player 1'))
    c.check_equal_file(game.try_get_trades('player 2'))


def test_fix_road(c):
    """Tests fixing a player's broken road"""
    c.check_equal_file(game.try_setup(c.folder))
    game.start()
    game.force_next_phase()
    # Break player's road (for this test, game force road to break)
    game.trades.break_road('player 2')
    c.check_equal_file(game.try_start_trade('player 1', 'player 2', {'A': 0}, {'B': 0}))
    # Players can't trade because of road
    c.check_equal_file(game.try_set_trade_state(0, 'player 2', 'OK'))
    # Fix a road not broken
    c.check_equal_file(game.try_fix_road('player 1', 'player 1'))
    # Player 1 is missing resource to fix road
    c.check_equal_file(game.try_fix_road('player 1', 'player 2'))
    # Player 2 has enough resource to fix road
    c.check_equal_file(game.try_fix_road('player 2', 'player 2'))
    c.check_equal_file(game.try_get_resources('player 2'))
    # Players can trade
    c.check_equal_file(game.try_set_trade_state(0, 'player 2', 'OK'))


def test_objectives(c):
    """Tests objectives distribution"""
    c.check_equal_file(game.try_setup(c.folder))
    c.check_equal_file(game.try_get_objectives('player 1'))
    c.check_equal_file(game.try_get_objectives('player 2'))
    c.check_equal_file(game.try_get_objectives('player 3'))


def test_initial_state(c):
    """Tests initial terrains distribution"""
    c.check_equal_file(game.try_setup(c.folder))
    game.start()
    c.check_equal_file(try_get_terrains_without_random('player 1'))
    c.check_equal_file(try_get_terrains_without_random('player 2'))
    c.check_equal_file(try_get_terrains_without_random('player 3'))


def test_housing(c):
    """Tests housing employees mechanic"""
    c.check_equal_file(game.try_setup(c.folder))
    game.start()
    game.force_next_phase()
    c.check_equal_file(game.try_house('player 1', -2))  # KO: negative number
    c.check_equal_file(game.try_house('player 1', 12))  # KO: Can't house more than capacity
    c.check_equal_file(game.try_house('player 1', 7))   # KO: Can't house more than number of employees
    c.check_equal_file(game.try_house('player 1', 2))   # OK
    game.force_next_turn()
    c.check_equal_file(game.try_get_resources('player 1'))  # Check new pollution count


def test_convert_rations(c):
    """Tests converting resources into rations"""
    c.check_equal_file(game.try_setup(c.folder))
    game.start()
    game.force_next_phase()
    c.check_equal_file(game.try_get_resources('player 1'))
    c.check_equal_file(game.try_convert_ration('player 1', 'housing', 1))
    c.check_equal_file(game.try_convert_ration('player 1', 'vegetable', -1))
    c.check_equal_file(game.try_convert_ration('player 1', 'vegetable', 1))
    c.check_equal_file(game.try_get_resources('player 1'))
    c.check_equal_file(game.try_convert_ration('player 1', 'vegetable', 10))
    c.check_equal_file(game.try_convert_ration('player 1', 'meat', 2))
    c.check_equal_file(game.try_get_resources('player 1'))


if __name__ == "__main__":
    test(test_game_setup, GAME_SETUP_FOLDER)
    test(test_activity, ACTIVITY_FOLDER)
    test(test_next_turn, NEXT_TURN_FOLDER)
    test(test_shared_resource, SHARED_RES_FOLDER)
    test(test_trade, TRADE_FOLDER)
    test(test_trade_impossible, TRADE_FOLDER)
    test(test_fix_road, TRADE_FOLDER)
    test(test_objectives, OBJECTIVES_FOLDER)
    test(test_initial_state, INIT_STATE_FOLDER)
    test(test_housing, EMPLOYEES_FOLDER)
    test(test_convert_rations, EMPLOYEES_FOLDER)
