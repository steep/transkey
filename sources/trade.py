#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import copy
import json
from player import Player
from utilities import log_info


class Trade:
    """Trade of resources between two users
    At the creation, the status of both player is 'PENDING'.
    At any time, if a player wants to cancel the trade (status 'KO'), it is aborted.
    When a player has enough resources, they can switch their status to 'OK' to notify the other player.
    They can also switch back to 'PENDING' state.
    If anybody edits the trade offer, both status get back to 'PENDING'.
    If a player updates his status and the other can't provide anymore the resources promised, the state gets back to 'PENDING'.
    When a trade is finished, we make sure each player has seen the outcome before removing trade from pending list.
    """

    def __init__(self, player1: Player, player2: Player, res_player1, res_player2):
        self.players: list[Player] = [player1, player2]
        self.resources = [res_player1, res_player2]
        self.status = ["PENDING", "PENDING"]  # Status is either "OK", "PENDING" or "KO"
        self.seen_outcome = [False, False]  # If player has received the info of the trade's outcome (when "KO" or both "OK")

    def __repr__(self):
        """Return trade info, with players list only containing names"""
        res = copy.deepcopy(self)
        res.players = [self.players[0].name, self.players[1].name]  # Show players names only

        return json.dumps(res.__dict__)

    def update_trade(self, player_name, own_res, other_res):
        """Update current trade resources
        Users are set back to "PENDING" state since resources requested/offered are changed"""

        if player_name == self.players[0].name:
            current_player_index = 0
            other_player_index = 1
        else:
            current_player_index = 1
            other_player_index = 0

        self.resources[current_player_index] = own_res
        self.resources[other_player_index] = other_res
        self.status[current_player_index] = "PENDING"
        self.status[other_player_index] = "PENDING"

    def finished(self):
        """Return true if trade is finished"""
        return self.status in [["OK", "OK"], ["KO", "KO"]]

    def update_seen_outcome(self, player_name):
        """Called when a player has seen the final outcome of a trade"""
        # Check if trade had a final outcome
        if not self.finished():
            return

        player_id = 0 if player_name == self.players[0].name else 1
        self.seen_outcome[player_id] = True

    def check_impossible_trade(self):
        """A trade for which a player promised resources they don't have anymore is set back to pending"""
        if self.finished():
            return

        for p in range(2):
            if self.status[p] == "OK" and not self.players[p].check_has_enough_resources(self.resources[p]):
                self.status[p] = "PENDING"
                self.status[1-p] = "PENDING"


class CurrentTrades:
    """Contains pending trades and players status"""

    def __init__(self):
        self.counter = 0  # ID of the next trade created
        self.pending = {}  # All pending trades, as a dictionary with ID as key
        self.broken_roads = set()  # Broken roads (player not allowed to trade)
        self.fix_road_cost = {}

    def add_trade(self, player1: Player, player2: Player, res1, res2):
        """Add new Trade to pending list"""
        trade_id = self.counter
        self.pending[trade_id] = Trade(player1, player2, res1, res2)
        self.counter += 1
        log_info("Trade", player1.name, f"{player1.name} is in trade {trade_id}")
        log_info("Trade", player2.name, f"{player2.name} is in trade {trade_id}")
        return trade_id

    def delete_trade(self, trade_id):
        """Removes Trade from pending list"""
        del self.pending[trade_id]

    def action_allowed(self, trade_id, player_name):
        """Check player action allowed (trade exists and players are in it)"""
        if trade_id not in self.pending:
            return False, f"Trade ID {trade_id} does not exist"
        trade = self.pending[trade_id]

        if player_name not in [trade.players[n].name for n in range(2)]:
            return False, f"Player {player_name} not in trade"

        # Check trade is not finished yet
        if trade.finished():
            return False, f"Trade already finished"

        return True, f"Action allowed"

    def try_start_trade(self, player1, player2, res1, res2, resources_dict) -> (bool, str):
        """Tries to start a new trade between two players with resources at initialization"""
        # Check every resource can be traded
        if not (can_trade(resources_dict, res1) and can_trade(resources_dict, res2)):
            return False, f"Some resources are not allowed to be traded"

        # REMOVED BEHAVIOUR (Allowed to announce resources player doesn't have, but can't accept trade)
        # # If player doesn't have the resources announced, no trade created
        # if not self.players[player1].check_has_enough_resources(res1):
        #     return False, f"Player {player1} is missing resources to initiate trade"

        # Add every resource which can be traded with a value of 0 to show client every resource
        # If this is changed, client also needs to be updated
        for res in resources_dict:
            if resources_dict[res].traded:
                if res not in res1:
                    res1[res] = 0
                if res not in res2:
                    res2[res] = 0

        trade_id = self.add_trade(player1, player2, res1, res2)
        return True, str(trade_id)

    def try_update_trade(self, trade_id, player_name, own_res, other_res, resources_dict) -> (bool, str | list):
        """Tries to update the trade offer (which resources are offered and requested)
        If successful, returns names of players involved in the trade"""
        # Check action allowed
        allow_status = self.action_allowed(trade_id, player_name)
        if not allow_status[0]:
            return allow_status

        # Check every resource can be traded
        if not (can_trade(resources_dict, own_res) and can_trade(resources_dict, other_res)):
            return False, f"Some resources can't be in a trade"

        self.pending[trade_id].update_trade(player_name, own_res, other_res)
        log_info("Trade", player_name, f"Trade {trade_id} updated")
        return True, [self.pending[trade_id].players[p].name for p in range(2)]

    def try_set_trade_state(self, trade_id, current_player, status, resources_dict) -> (bool, str | dict):
        """
        Updates trade stade
        Returns:
        If a player has a broken road: False, error_message
        If a player has missing resources: False, error_message
        If trade is not done yet: True, {"names": players_names, "trade_done": False}
        If trade is now done: True, {"names": players_names, "trade_done": True}
        """
        # Check action allowed
        allow_status = self.action_allowed(trade_id, current_player)
        if not allow_status[0]:
            return allow_status

        trade = self.pending[trade_id]
        players_names = [trade.players[p].name for p in range(2)]
        player_id = 0 if current_player == trade.players[0].name else 1
        other_player_id = 1 if current_player == trade.players[0].name else 0
        match status:
            case "OK":

                # Check players are allowed to trade
                # If there's at least a broken road between them, they can only trade allowed resources
                if trade.players[0].name in self.broken_roads or trade.players[1].name in self.broken_roads:
                    # If forbidden resource, impossible trade
                    for player_res in trade.resources:
                        for res in player_res:
                            if not resources_dict[res].traded_broken and player_res[res] > 0:
                                return False, f"{res} can't be traded with broken road"

                # If player doesn't have enough resources, "OK" is refused
                if not trade.players[player_id].check_has_enough_resources(trade.resources[player_id]):
                    return False, f"Player '{current_player}' is missing resources for the trade with '{trade.players[other_player_id].name}'"

                trade.status[player_id] = "OK"
                # If other player isn't "OK" yet, return
                if not trade.status[other_player_id] == "OK":
                    return True, {"names": players_names, "trade_done": False}

                # If other player can't do the trade anymore, can't validate
                # Other player is back to "PENDING" because they can't accept without having the resources
                if not trade.players[other_player_id].check_has_enough_resources(trade.resources[other_player_id]):
                    trade.status[other_player_id] = "PENDING"
                    return False, f"Player '{trade.players[other_player_id].name}' is missing resources for the trade with '{current_player}'"

                # Trade done: resources are swapped and other trades checked
                for p in range(2):
                    player: Player = trade.players[p]
                    player.subtract_resources_inventory(trade.resources[p])
                    player.add_resources_inventory(trade.resources[1 - p])
                    player.auto_house_trade(resources_dict, trade.resources[1 - p].get("employee", 0))

                # Log trade done and new resources
                for p in range(2):
                    log_info("Trade", trade.players[p].name, f"Trade {trade_id} done")
                    log_info("Resources", trade.players[p].name, trade.players[p].get_resources(resources_dict))

                # Add to data visualizations
                for p in range(2):
                    player = trade.players[p]
                    player.data_visu.add_trade(player.resources, trade.players[1-p].name, trade.resources[p], trade.resources[1 - p])
                    player.data_trade_visu.add_trade(trade.players[1-p].name, trade.resources[p], trade.resources[1 - p])

                return True, {"names": players_names, "trade_done": True}
            case "PENDING":
                trade.status[player_id] = "PENDING"
                return True, {"names": players_names, "trade_done": False}
            case "KO":
                trade.status[player_id] = "KO"
                trade.status[other_player_id] = "KO"
                for p in range(2):
                    log_info("Trade", trade.players[p].name, f"Trade {trade_id} canceled")
                return True, {"names": players_names, "trade_done": False}
            case _:
                return False, f"Status {status} does not exist for trades"

    def set_fix_road_cost(self, fix_road_cost):
        """Set road repair cost, as a dictionary with resource name and quantities"""
        self.fix_road_cost = fix_road_cost

    def get_fix_road_cost(self):
        """Get road repair cost"""
        return self.fix_road_cost

    def break_road(self, player_name):
        """Add player to broken roads list"""
        self.broken_roads.add(player_name)

    def get_broken_roads(self):
        """Returns broken roads as a list"""
        return list(self.broken_roads)

    def try_fix_road(self, current_player: Player, isolated_player_name) -> (bool, str):
        """Tries to repair the road of an isolated player (can be themselves)"""
        # Check player's road is broken
        if isolated_player_name not in self.broken_roads:
            return False, f"Road of {isolated_player_name} not broken"

        # Check has enough resources
        if not current_player.check_has_enough_resources(self.fix_road_cost):
            return False, f"Missing resource to fix the road of '{isolated_player_name}'"

        current_player.subtract_resources_inventory(self.fix_road_cost)
        current_player.data_visu.add_road_repair(current_player.resources, self.fix_road_cost)
        current_player.data_trade_visu.add_fix_road(self.fix_road_cost)
        self.broken_roads.remove(isolated_player_name)
        return True, f"Road of {isolated_player_name} repaired"

    def get_trades(self, player_name):
        """Returns a list of pending trades for the player requested, with content in json format"""
        trades_list = []
        for t in self.pending:
            if player_name in [self.pending[t].players[p].name for p in range(2)]:
                # Add trade info and trade ID
                trades_list.append('{"id":' + str(t) + ", " + str(self.pending[t])[1:-1] + "}")
                self.pending[t].update_seen_outcome(player_name)  # Update seen outcome status if necessary
        self.remove_done_trades()
        return trades_list

    def remove_done_trades(self):
        """Remove all finished trades with outcome seen by both players"""
        for t in copy.deepcopy(list(self.pending.keys())):
            # For each trade with outcome seen by both players, remove trade from pending list
            if self.pending[t].seen_outcome == [True, True]:
                del self.pending[t]

    # TODO CHECK HOW USER COULD BE UPDATED CORRECTLY AFTER THIS (Currently calls for trades updates anyway)
    def check_impossible_trades(self):
        """A trade for which a player promised resources they don't have anymore is set back to pending"""
        for t in self.pending:
            self.pending[t].check_impossible_trade()


def can_trade(resources_dict, user_res_dict):
    """Returns True if every resource of a dictionary is allowed to be traded in normal conditions"""
    for res in user_res_dict:
        if res not in resources_dict:  # Resource doesn't exist
            return False
        if not resources_dict[res].traded:  # Resource is not allowed to be traded in normal conditions
            return False
    return True
