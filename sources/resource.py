#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import json


class ResourceInfo:
    """Contains properties about a resource
    traded: boolean to know if resource can be used in trades
    traded_broken: boolean to know if resource can be used in trades even when road is broken
    shared: boolean to know if resource is shared between all players
    reset: boolean to know if the resource number is reset between turns
    ration_value: int of how many rations the player gets when converting
    visible: boolean to know if the resource is seen by other players
    category: str of category for display
    color: color used for display
    image_path: image path used for the web client
    """

    def __init__(self, res_info, image_path):
        self.traded = False
        self.traded_broken = False
        if res_info.get("traded"):
            self.traded = res_info.get("traded").get("normal", False)
            self.traded_broken = res_info.get("traded").get("broken", False)

        self.shared = res_info.get("shared", False)
        self.reset = res_info.get("reset", False)
        self.visible = res_info.get("visible", False)
        self.ration_value = res_info.get("ration value", 0)
        self.category = res_info.get("category", "")
        self.color = res_info.get("color", "#000000")

        self.image_path = image_path

    def __repr__(self):
        """Used for get_resources_info"""
        return json.dumps(self.__dict__)
