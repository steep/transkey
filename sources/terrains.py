#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from activity import Activity
from z3 import *
from utilities import print_dict


class Terrain:
    """Store a terrain with its activity, size and position"""

    def __init__(self, activity: Activity):
        self.act = activity
        self.size = [0, 0]
        self.pos = [0, 0]

    def __repr__(self):
        return print_dict(self.__dict__)

    def update_size(self, w_and_h):
        self.size = w_and_h

    def update_pos(self, x_and_y):
        self.pos = x_and_y


class Terrains:
    """
    Contains terrains of a given territory
    They are stored in a grid, with each activity having a size and position
    """

    def __init__(self, nb_terrains):
        """
        width: total width of displayed terrains
        height: total height of displayed terrains
        terrains: list of activities placed on territory
        """
        self.width = 5
        self.height = nb_terrains // 5
        self.terrains: list[Terrain] = []

    def __repr__(self):
        return str(self.terrains)

    def get_terrains(self) -> (list[Terrain], tuple[int, int]):
        """Return player's terrains"""
        return self.terrains, (self.width, self.height)

    def try_add_activity(self, activity) -> (bool, str):
        """Try to add a new activity on terrains taking into account the size of each activity"""

        # Create an array which length is the number of activities and each element is the length of an activity
        pieces_len_array = []
        for act in self.activities_iterator():
            pieces_len_array.append(act.info.terrains_req)
        pieces_len_array.append(activity.info.terrains_req)

        if sum(pieces_len_array) > self.width * self.height:
            return False, f"Not able to fit new activity in terrains"

        # Extra constraints: keep every activity at the same spot before adding another one
        extra_constraints = {"w": {}, "h": {}, "x": {}, "y": {}}
        for i, t in enumerate(self.terrains):
            extra_constraints["w"][i] = t.size[0]
            extra_constraints["h"][i] = t.size[1]
            extra_constraints["x"][i] = t.pos[0]
            extra_constraints["y"][i] = t.pos[1]

        # Call solver with every constraint
        res = self._solve_activities_layout(pieces_len_array, extra_constraints=extra_constraints)
        # If not successful, call solver with fewer constraints
        if not res[0]:
            res = self._solve_activities_layout(pieces_len_array)
        # If still not successful, impossible case so we abort
        if not res[0]:
            return res

        # If solved, append to terrains and update every size and position
        self.terrains.append(Terrain(activity))
        for act_id, terrain in enumerate(self.terrains):
            x, y = res[1].evaluate(Int(f"x[{act_id}]")), res[1].evaluate(Int(f"y[{act_id}]"))
            w, h = res[1].evaluate(Int(f"w[{act_id}]")), res[1].evaluate(Int(f"h[{act_id}]"))

            terrain.update_pos([x, y])
            terrain.update_size([w, h])

        return True, f"Solution found"

    def remove_activity(self, act_id):
        """Remove activity from terrains"""
        self.terrains = self.terrains[:act_id] + self.terrains[act_id + 1:]

    def get_nb_occurrences(self, activity_name):
        """Return number of times an activity is already on the territory"""
        nb = 0
        for terrain in self.terrains:
            if terrain.act.name == activity_name:
                nb += 1
        return nb

    def get_act(self, terrain_id):
        """Get activity from terrain_id"""
        return self.terrains[terrain_id].act

    def activities_iterator(self):
        for t in self.terrains:
            yield t.act

    def _solve_activities_layout(self, pieces_len_array, extra_constraints=None) -> (bool, str | z3.ModelRef):
        """Tries to fit every pieces in player's terrains
        extra_constraints can be sent as a dictionary with 4 elements
        (w, h, x and y) each containing a dictionary associating an Int to its forced value
        (example : {w:{0: 1}} will force w[0] to be equal to 1)"""
        s = Solver()

        # w and h are width and height of each piece
        w = [Int(f'w[{i}]') for i in range(len(pieces_len_array))]
        h = [Int(f'h[{i}]') for i in range(len(pieces_len_array))]

        # x and y are top-left coordinates of each piece
        x = [Int(f'x[{i}]') for i in range(len(pieces_len_array))]
        y = [Int(f'y[{i}]') for i in range(len(pieces_len_array))]

        # Add constraints for each piece
        for i, p in enumerate(pieces_len_array):

            # Size possibilities in both directions
            s.add(Or([Or(And(w[i] == res[0], h[i] == res[1]),
                         And(w[i] == res[1], h[i] == res[0])) for res in activity_shape(p)]))

            # Fit on terrains
            s.add(x[i] >= 0)
            s.add(x[i] + w[i] <= self.width)
            s.add(y[i] >= 0)
            s.add(y[i] + h[i] <= self.height)

            # No overlap with previous pieces
            for i2, p2 in enumerate(pieces_len_array[:i]):
                s.add(Or(x[i] + w[i] <= x[i2],
                         x[i2] + w[i2] <= x[i],
                         y[i] + h[i] <= y[i2],
                         y[i2] + h[i2] <= y[i]))

        # Add extra constraints
        if extra_constraints:
            for name, array in [["x", x], ["y", y], ["w", w], ["h", h]]:
                if name not in extra_constraints:
                    continue
                for terrain_id in extra_constraints[name]:
                    s.add(array[terrain_id] == extra_constraints[name][terrain_id])

        if s.check() != sat:
            return False, f"Not able to fit new activity in terrains"
        else:
            return True, s.model()


def activity_shape(size):
    """Find the shape which is the closest to a square"""
    res = [size, 1]
    i = 2
    while i ** 2 <= size:
        if not size % i:
            res = [size // i, i]
        i += 1
    return [res]
