#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""Define logging and common functions used by many files"""

import logging

# Logging configuration
logging.basicConfig(filename='transkey.log', level=logging.INFO,
                    format='%(asctime)s # %(levelname)s # %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def log_info(log_type, player, message):
    """Add a new info to the log file
    Player can be "Server" if it's about game setup"""
    logging.info(f"{log_type} # {player} # {message}")


def print_dict(dictionary, function=lambda dic, elem: dic[elem]):
    """Print a dictionary as a json string with double quotes
    function can be used to only print a part of the key"""
    arr = ['"' + str(key).replace('"', "'") + '":' + str(function(dictionary, key)) for key in dictionary]
    return "{" + ",".join(arr) + "}"
