#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import matplotlib.pyplot as plt
from sankeyflow_edit import Sankey
from copy import deepcopy


class Transfo:
    """Transformation process (activity OR trade)"""

    def __init__(self, name, res_in, res_out):
        """for Sankey graph, we need transformation's name, input and output"""
        self.name = name
        self.res_in = res_in
        self.res_out = res_out


class DataVisu:
    """Contains every data useful for generating a sankey graph of player's turn"""

    def __init__(self, initial_res, resources_dict):
        self.res = deepcopy(initial_res)  # Save resources inventory
        self.transfo = None  # Save current transformation
        self.flows = []  # Every flow, following the format ["origin_i", "dest_i+1", qt, "resource"]
        self.nb_step = 1
        # Set of resources to shown in Sankey flows
        self.shown_resources = set(filter(lambda r: resources_dict[r].visible, resources_dict))

    def add_step(self, new_res, new_transfo):
        """Add new resources transformation (activity OR trade),
        takes resources as input and outputs other resources"""

        # For each resource, computes different values of stocks and activities to create flows
        for r in self.shown_resources:
            if r in self.shown_resources:
                old_act_val = max(0, self.transfo.res_out.get(r, 0) if self.transfo else 0)
                new_act_val = new_transfo.res_in.get(r, 0)
                # If negative value for a product
                if new_transfo.res_out.get(r, 0) < 0:
                    new_act_val -= new_transfo.res_out.get(r, 0)
                old_stock = self.res.get(r, 0) - old_act_val
                new_stock = new_res.get(r, 0)

                # Stock flow
                if old_stock:
                    self.add_flow(r, r, r, min(old_stock, new_stock))

                # Activity flow
                if self.transfo:
                    self.add_flow(self.transfo.name, new_transfo.name, r, min(old_act_val, new_act_val))

                # Stock to activity OR activity to stock flow
                # Since the "stock to stock" and "activity to activity" have the highest values possible,
                # only one flow of the two following remains:
                # - "stock to activity" (new activity needs stock)
                # - "activity to stock" (previous activity create more resource than needed for new activity)
                if self.transfo and old_act_val > new_act_val:  # "activity to stock" flow
                    self.add_flow(self.transfo.name, r, r, old_act_val - new_act_val)
                elif new_stock < old_stock:  # "stock to activity" flow
                    self.add_flow(r, new_transfo.name, r, old_stock - new_stock)

        # Add fake flow to make sure render does not crash (Ugly code)
        self.add_flow("water", new_transfo.name, "water", 0)

        # Save for next step
        self.res = deepcopy(new_res)
        self.transfo = new_transfo
        self.nb_step += 1

    def add_act(self, new_res, new_activity):
        new_transfo = Transfo(new_activity.name, new_activity.info.turn_req, new_activity.info.turn_prod)
        self.add_step(new_res, new_transfo)

    def add_trade(self, new_res, other_player_name, player_res, other_player_res):
        trade_transfo = Transfo(f'trade with "{other_player_name}"', player_res, other_player_res)
        self.add_step(new_res, trade_transfo)

    def add_road_repair(self, new_res, recipe):
        road_repair_transfo = Transfo("road repair", recipe, {})
        self.add_step(new_res, road_repair_transfo)

    def add_rations(self, new_res, food_converted, rations):
        ration_transfo = Transfo(f"food converted to rations", food_converted, rations)
        self.add_step(new_res, ration_transfo)

    def add_give_res(self, new_res, res_dict):
        give_res_transfo = Transfo(f"resources received", {}, res_dict)
        self.add_step(new_res, give_res_transfo)

    def add_inventory(self):
        """Used to add flows for current stocks
        Should only be called once (at the end)!"""

        # For each resource, computes different values of stocks to create flows with previous step
        for r in sorted(list(set(self.res))):
            if r in self.shown_resources:
                old_act_val = max(0, self.transfo.res_out.get(r, 0) if self.transfo else 0)  # Only if positive output
                stock = self.res.get(r, 0) - old_act_val

                # Stock flow
                if stock:
                    self.add_flow(r, r, r, stock)

                # "Activity to stock" (previous activity creates resources)
                if self.transfo and old_act_val:
                    self.add_flow(self.transfo.name, r, r, old_act_val)

        self.nb_step += 1
        self.transfo = None

    def add_flow(self, beg, end, res, qt):
        """Add new flow to list following the format ["origin_i", "dest_i+1", qt, "resource"]"""
        self.flows.append([f"{beg}_{self.nb_step}", f"{end}_{self.nb_step + 1}", max(qt, 1e-200), f"{res}"])

    def render_sankey(self, res_dict, activities_dict):
        """Generates Sankey flow diagram"""
        self.add_inventory()  # Add inventory for final column

        # -- Creates matplotlib figure and diagram
        plt.figure(figsize=(self.nb_step * 3, 12))
        s = Sankey(flows=self.flows, res_dict=res_dict, act_dict=activities_dict,
                   node_pad_y_min=0.04, align_y='center',
                   node_opts={'label_pos': 'top',
                              'label_opts': dict(fontsize=10),
                              'label_format': '{label}'},
                   flow_opts={'placement_ratio': 0.99})
        s.draw()
        plt.title("")
        return plt
