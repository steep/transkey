#!/usr/bin/env python

# 
# This file is part of the Transkey distribution (https://gitlab.inria.fr/steep/transkey/).
# Copyright (c) 2024 Inria.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import matplotlib.pyplot as plt
from sankeyflow_edit import Sankey
from utilities import print_dict


class TradeVisu:
    """Data used for trades visualization on game canvas
    Gives "sum" data to client and imports/exports details for detailed Sankeys"""

    def __init__(self):
        self.values: dict | dict[TradeVisuRes] = {}  # For each resource, store a TradeVisuRes

    def __repr__(self):
        return print_dict(self.values)

    def add_trade(self, other_player_name, player_res, other_player_res):
        """Updates values of import/export of every resource depending on trade values"""
        # EXPORT
        for res in player_res:
            if res not in self.values:
                self.values[res] = TradeVisuRes()
            self.values[res].add_export(other_player_name, player_res[res])

        # IMPORT
        for res in other_player_res:
            if res not in self.values:
                self.values[res] = TradeVisuRes()
            self.values[res].add_import(other_player_name, other_player_res[res])

    def add_fix_road(self, recipe):
        """Updates values of export when fixing road"""
        self.add_trade("Road repair", recipe, {})

    def add_shared(self, shared_res, val):
        """Updates values of import/export when modifying shared resource value"""
        if val > 0:
            self.add_trade("Global", {shared_res: val}, {})
        elif val < 0:
            self.add_trade("Global", {}, {shared_res: -val})

    def try_render_trade_sankey(self, res_name, res_dict, folder, filename) -> (bool, str):
        """Generates Sankey flow diagram for a given resource"""

        if res_name not in self.values:
            return False, f"Resource {res_name} not found for this player"

        # Get flows in the format of ["origin_i", "dest_i+1", qt, "resource"]
        imports, exports = self.values[res_name].get_flows(res_name)

        # -- Creates matplotlib figures and diagrams
        _, (imp_plot, exp_plot) = plt.subplots(1, 2, figsize=(12, 8))

        for title, flows, plot in [["Imports", imports, imp_plot], ["Exports", exports, exp_plot]]:
            plt.sca(plot)
            plt.title(title)

            s = Sankey(flows=flows, res_dict=res_dict,
                       node_pad_y_min=0.04, align_y='center',
                       node_opts={'label_pos': 'top',
                                  'label_opts': dict(fontsize=10),
                                  'label_format': '{label}'},
                       flow_opts={'placement_ratio': 0.99})
            s.draw()

        plt.savefig(os.path.join(folder, filename))
        plt.close()

        return True, filename


class TradeVisuRes:
    """For a given resource, store its displayed value on terrains and details of imports/exports"""
    def __init__(self):
        self.sum = 0
        self.imports = {}
        self.exports = {}

    def __repr__(self):
        return str(self.sum)

    def add_import(self, other_player_name, val):
        if val == 0:
            return

        # Subtract from sum
        self.sum -= val

        # Add to imports
        other_player_name += "_"  # Add underscore because sankeyflow_edit removes everything after last underscore
        if other_player_name not in self.imports:
            self.imports[other_player_name] = 0
        self.imports[other_player_name] += val

    def add_export(self, other_player_name, val):
        if val == 0:
            return

        # Add to sum
        self.sum += val

        # Add to exports
        other_player_name += "_"  # Add underscore because sankeyflow_edit removes everything after last underscore
        if other_player_name not in self.exports:
            self.exports[other_player_name] = 0
        self.exports[other_player_name] += val

    def get_flows(self, res_name):
        """Get flows for Sankey generation"""

        imp = [[i, "Imports", self.imports[i], res_name] for i in self.imports]
        exp = [["Exports", e, self.exports[e], res_name] for e in self.exports]

        return imp, exp
