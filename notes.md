# Transkey version multijoueur - Conception

## Choix d'implémentation et infos diverses

- Utilisation de `Flask` (backend et transmission des pages frontend) et `socketio` (API et sockets pour une communication instantanée bidirectionnelle) sur Python.
La création d'API permet d'avoir une construction plus claire aux niveaux des attentes et objectifs.
- Frontend développé avec HTML/CSS et JavaScript (utilisation de `Konda` pour le rendu graphique).
- Le serveur n'a qu'une seule partie en cours et peut charger une configuration dès que souhaité.
C'est lui qui contient l'ensemble des informations et les communique aux joueurs via les sockets.
- Une architecture MVC est utilisée. Le contrôleur appelle le modèle via les fonctions de l'objet game commençant par le préfixe `try_`. Ces fonctions renvoient un tuple dont le premier élément est un booléen indiquant le succès de la demande et la suite contient un message d'erreur en cas d'échec ou des informations pertinentes pour l'envoi d'informations à la vue (donc au client).
- Lorsqu'une ressource du jeu est consommée / produite, le modèle vérifie d'abord si elle est dans les ressources communes avant de vérifier si le joueur en possède en tant que ressource personnelle. De plus, il est impossible pour une ressource de passer dans les négatifs.
- Les objectifs individuels et les terrains initiaux sont répartis aléatoirement en prenant en compte la liste des joueurs avec lesquels ils sont compatibles (cf configuration des fichiers). Ils ne sont pas calculés automatiquement par le jeu, c'est au joueur de vérifier s'il atteint ou non ses objectifs.
- Les langues et traductions sont disponibles sur les clients web en notant dans un fichier json les traductions de chaque texte. Chaque élément textuel peut être défini via une balise `data-i18n` ou alors son ID est directement récupéré dans les fichiers javascript. Pour forcer le rechargement de l'ensemble des textes d'une langue, ouvrir la console JavaScript sur le navigateur et écrire `localStorage.clear()`
- Les modifications de règles propres à la version numérique sont dans [ce markdown](./regles.md).
- Les diagrammes de Sankey pourraient être générés du côté du client pour certains d'entre eux, mais la méthode actuelle permet de les exporter et de les conserver ce qui permet une analyse après la partie. Pour le moment, il s'agit d'un fork de la librairie `sankeyflow` à laquelle il y a eu des ajouts (couleurs, images, ...). Les flows sont sous la forme _["nom\_origine\_i", "nom\_destination\_i+1", quantité, "nom\_ressource"]_ avec i le numéro de l'étape du diagramme

## TODO
_Implémentations / suggestions pour la version numérique. S'il a un "?", ce sont des pistes de réflexion_

- Les "TODO" présents dans les fichiers concernent des points techniques non résolus / en suspens (faire `grep -r -i 'todo' à la racine` pour les voir)

- (?) Sur l'interface web, afficher les catégories des activités comme pour les ressources (Dans les data transmises, il y a déjà l'info)

- Visualisations
  - (?) Normaliser l'épaisseur au sein d'une ressource pour éviter de les comparer entre elles (cf document bilan)
  - Sankey global ou meilleure interface pour naviguer entre les différents diagrammes
  - Meilleur affichage quand il s'agit d'une ressource globale (afficher les acitivités qui ont contribué et absorbé la ressource / l'indicateur)

- Avoir des logs fonctionnels

- Chats :
  - (?) Ajout d'un historique du chat si quelqu'un rafraichit la page
  - Ajout d'un chat privé

- Langues
  - Message du backend traduits selon client (mais garder les logs en anglais)
  - Traduire certains terme pour les rendus avec un diagramme de Sankey
  - Noms de ressources / activités traduits selon langue du client (et non du serveur ?)

- Améliorer l'ergonomie de la page de connexion

- Frontend en React

- Utiliser l'_Object serialization_ pour avoir des backup si nécessaire

- Déployer le jeu sur le réseau global pour le rendre accessible de partout

- (?) Utiliser OpenAPI pour générer la doc

## Fichiers de configuration (Contenu des YAML)
Les fichiers de configuration sont tous contenus dans un même dossier.

- `config_game.yaml` contient les infos générales sur le jeu :
  - `activities categories` (facultatif) : chaque élément est une catégorie des activités. A chacune on attribue un texte qui est la couleur utilisée (nom ou code hexadécimal).
  - `resources categories` (facultatif) : liste des catégories des ressources et indicateurs.
  - `visible info` (facultatif) : chaque élément indique si un type d'information est accédé par les joueurs. De base, tout est considéré `False`. *Pour ajouter de nouveaux éléments, aucun code à modifier dans le backend*.
    - `other resources` (facultatif) : si `True`, les ressources des autres joueurs sont affichées sur leur territoire.
    - `terrains visualizations` (facultatif) : si `True`, les flux des terrains sont affichés pendant le tour.
  - `road repair cost` (facultatif): liste sous forme de dictionnaire qui indique les ressources et quantités nécessaires à la réparation d'une route cassée
  - `turns` (facultatif) : nombre de tours joués. Considéré `1` de base
___
- `config_resources.yaml` : liste des ressources et de leurs propriétés (doit contenir *toutes* les ressources du jeu). Les terrains ne sont pas considérés comme une ressource car ils ont une catégorie à part dans `config_activities.yaml` et sont représentés par la place que prennent les activités. Pour chaque ressource, on a les catégories suivantes:
  - `image` (facultatif) : chemin pour trouver l'image à afficher
  - `traded` (facultatif) : contient deux sous-catégories pour autorisation à l'échange selon l'état des routes
    - `normal` (facultatif) : si `True`, la ressource peut être échangée entre les joueurs. Considéré `False` de base
    - `broken` (facultatif) : si `True`, la ressource peut être échangée même si les routes sont cassées. Considéré `False` de base
  - `reset` (faculatif): si `True`, la ressource est remise à une valeur de 0 entre les tours. Considéré `False` de base
  - `shared` (facultatif) : si existant, la ressource/métrique (et son nombre) est partagée entre les joueurs. Sa valeur au lancement du jeu est celle indiquée. Il faut considérer que ces ressources ne sont donc pas échangeables
  - `ration value` (faculatif) : si existant, indique le nombre de rations obtenus pour chaque unité convertie. Considéré à `0` de base. _Note : La ressource "ration" est alors supposée existante_
  - `visible` (facultatif) : si `True`, la ressource et sa quantité sont affichés sur le territoire et visible par les autres joueurs (sauf si un critère de `visible info` dans `config_game.yaml` indique le contraire). De plus, la ressource est affichée dans les visualisations de flux. Considéré `False` de base
  - `color` (facultatif) : couleur utilisée pour les visualisations, considéré noir (`#000000`) de base.
  - `category` (facultatif) : catégorie associée (doit exister dans `resources categories` dans ̀`config_game.yaml`)

**A noter :** Si les ressources "employee", "housed employee", "greenhouse gas" et "housing" existent, alors la mécanique d'hébergement est appliquée et les employés non hébergés polluent en fin de tour.
___
- `config_players.yaml` permet d'indiquer la situation initiale de chaque joueur.
  - `max actions` (facultatif): indique le nombre d'aménagements maximaux effectuées chaque tour. Considéré `0` de base
  - `terrains` (facultatif): nombre de terrains possédé par le joueur. Considéré à `0` de base
  - `resources` (facultatif): indique sous la forme d'un dictionnaire les ressources du joueur et leurs nombres
  - `new turn resources` (facultatif): indique les ressources perçues par le joueur en début de tour
  - `description` (facultatif): texte affiché pour connaître les spécificités du territoire.
___
- `config_activities.yaml` contient les activités. Chacune a plusieurs catégories:
  - `image` (facultatif) : chemin pour trouver l'image à afficher
  - `category` (facultatif) : catégorie associée (doit exister dans `activities categories` dans ̀`config_game.yaml`)
  - `recipes` : Contient l'ensemble des recettes pour créer et gérer l'activité. La sous-catégorie `all` est la recette appliquée de base, si elle existe. Si une configuration pour un joueur existe (sous-catégorie avec son nom dans `config_players.yaml`), elle est appliquée en priorité. Pour chaque recette, on a l'ensemble des informations suivantes (toutes facultatives) :
      - `allowed` : si égal à `False`, affiche au joueur la recette `all` de l'activité si elle existe mais ne peut être ajoutée
      - `auto` : L'activité est automatiquement utilisée en fin de tour 
      - `actions cost` : coût en nombre d'actions pour placer l'activité, considéré `1` sinon
      - `terrains cost` : coût du nombre de terrains pour l'installation, considéré `1` sinon
      - `turn requirements` : coût pour avoir de la production pour un tour (obtenue instantanément)
      - `turn products` : produits obtenus instantanément s'il y a production
      - `max number` : nombre maximum de ce type d'activité sur un territoire, considéré infini sinon
___
- `config_objectives.yaml` (facultatif) contient les objectifs des joueurs.
  - `shared`: Objectif commun à tous les joueurs.
  - `individual`: Contient une liste d'objectifs (noms arbitraires non utilisés) avec, pour chacun, plusieurs informations :
    - `description`: Description textuelle de l'objectif
    - `compatibility`: Liste des joueurs qui peuvent obtenir cet objectif (pour éviter les incompatibilités)
___
- `config_initial_state.yaml` (facultatif) contient les activités placées dès le début de la partie. Contient une liste d'objectifs (noms arbitraires non utilisés) avec, pour chacun, plusieurs informations :
    - `activities`: Liste des activités ajoutées
    - `compatibility`: Liste des joueurs qui peuvent obtenir ce kit de départ (pour éviter les incompatibilités)

**A noter :** Le choix de ne pas calculer automatiquement les compatibilités a été fait pour les cas où on souhaite restraindre certains couplages de villages / activités au-delà des restrictions dues aux règles

## Tests du modèle

Un fichier `test.py` permet de vérifier les fonctionnalités du jeu.
Plusieurs dossiers de config sont dans `tests_configs` et contiennent
les fichiers utilisés pour les tests ainsi que ceux pour vérifier que les résultats correspondent aux attentes. Ils ont pour but principal d'être des tests de non-régression et de simuler des scénarios réalistes, ainsi la couverture du code n'est pas forcément totale puisqu'on souhaite surtout vérifier l'implémentation des mécaniques.

## Mise en place

Étapes à suivre pour jouer une partie:
- Créer un environnement Python avec une version récente (`3.12` recommandée) (`conda create -n NOM python=3.12`)
- Installer les dépendances (`pip install -r requirements.txt`)
- Lancer le serveur depuis le dossier `sources` (`python server.py`) (option `-h` pour voir les arguments qu'on peut fournir)
- Aller sur `http://{ADDRESSE_IP}:5000/` pour jouer (l'addresse est localhost en local, ou à trouver via `ifconfig` sinon. Vérifier que le port 5000 est correctement ouvert si la connexion est impossible)
- Pour avoir accès aux commandes d'administrateur, aller sur la page accessible depuis le menu. Ici, il est possible de charger une configuration en mettant son path relatif à l'endroit d'exéction du serveur puis une fois que les joueurs sont renommés et prêts, cliquer sur _Start game_ une fois. Chacun peut ensuite rejoindre la partie avec _Join_.

## API
L'API est définie dans [ce markdown](./api.md).