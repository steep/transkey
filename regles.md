# Transkey version multijoueur - Règles en plus pour la version numérique

(Les règles générales peuvent être trouvées dans le document guide)

## Ajout aux règles du jeu pour la version numérique

- Le joueur peut indiquer quand il a fini la phase en cours.
  Il peut aussi annuler cet état en cliquant à nouveau sur le bouton ou en effectuant une action.
  Si tous les joueurs ont indiqué avoir fini leur tour, le jeu passe à la phase suivante.
  Les joueurs ont l'information du nombre de joueurs prêts à passer à la phase suivante.
- Les échanges de ressources ont lieu via une interface et il faut obtenir un consensus biparti pour les percevoir.

- Une mécanique d'auto-hébergement est présente, car il est très rare - voire inexistant - qu'un joueur décide volontairement de ne pas héberger sur son territoire.
  - A chaque ajout de capacité d'hébergement pour n employés, jusqu'à n employés sont ajoutés automatiquement à l'hébergement
  - A chaque ajout de n employés via une activité, jusqu'à n employés sont ajoutés automatiquement à l'hébergement
  - Au niveau des échanges, la capacité d'hébergement est cappée aux nombres d'employés restants si ce nombre diminue, mais les employés récupérés ne sont pas hébergés automatiquement
