# docker build -t transkey .
# docker run --name transkey -p 5000:5000 -it --rm transkey
FROM ubuntu:22.04
RUN apt-get update && apt-get upgrade -y
RUN apt-get update 
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get install -y build-essential software-properties-common 
RUN apt-get update 
RUN add-apt-repository ppa:deadsnakes/ppa &&\
    apt-get update &&\
    apt-get install -y python3-pip
        
# switch working directory
COPY . /app
WORKDIR /app/sources

RUN pip install -r ../requirements.txt 

# configure the container to run in an executed manner
ENTRYPOINT [ "python3.10" ]
CMD ["server.py" ]